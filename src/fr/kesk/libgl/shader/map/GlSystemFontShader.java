package fr.kesk.libgl.shader.map;

import java.io.InputStream;

import fr.kesk.libgl.shader.map.GlAbstractFontMapShader;

/**
 * Simple font map shader to use mainly for debugging purpose 
 * 
 * @author Thomas MILLET
 *
 */
public class GlSystemFontShader extends GlAbstractFontMapShader {

	/**
	 * TAG log
	 */
	@SuppressWarnings("unused")
	private final static String TAG = GlSystemFontShader.class.getName();
	
	/**
	 * Location of font map
	 */
	private final static String FONT_MAP = "/fr/kesk/libgl/shader/gsgl/map/assets/font.png";
	
	/**
	 * Location of font map metadata
	 */
	private final static String FONT_MAP_METADATA = "/fr/kesk/libgl/shader/gsgl/map/assets/font.map";
	
	
	/* (non-Javadoc)
	 * @see fr.kesk.libgl.shader.map.GlAbstractFontMapShader#setText(java.lang.String)
	 */
	@Override
	public void setText(String text) {
		//TODO
		super.setText(text);
	}

	/* (non-Javadoc)
	 * @see fr.kesk.libgl.shader.map.GlAbstractFontMapShader#getFontMapInputStream()
	 */
	@Override
	protected InputStream getFontMapInputStream() {
		return this.getClass().getResourceAsStream(FONT_MAP);	
	}

	/* (non-Javadoc)
	 * @see fr.kesk.libgl.shader.map.GlAbstractFontMapShader#getFontMetadataInputStream()
	 */
	@Override
	protected InputStream getFontMetadataInputStream() {
		return this.getClass().getResourceAsStream(FONT_MAP_METADATA);
	}

	

}