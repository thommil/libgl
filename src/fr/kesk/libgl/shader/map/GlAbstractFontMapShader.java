package fr.kesk.libgl.shader.map;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES20;
import android.opengl.GLException;
import android.util.SparseIntArray;

import fr.kesk.libgl.GlContext;
import fr.kesk.libgl.GlAssets.Node;
import fr.kesk.libgl.buffer.GlBuffer;
import fr.kesk.libgl.shader.GlProgram;
import fr.kesk.libgl.shader.GlShader;
import fr.kesk.libgl.texture.GlTexture;
import fr.kesk.libgl.tools.ByteBufferPool;
import fr.kesk.libgl.tools.GLES20Utils;

/**
 * Shader dedicated to font display based on LibGL format :
 * <ul>
 * 	<li>Font map image file</li>
 * 	<li>Font map metadata file</li>
 * </ul>	
 * Implementations must at least give access to font resources and should override
 * setText() method to format text before rendering. This class can also be used for drawing
 * tiles.
 *
 * @author Thomas MILLET
 *
 */
public abstract class GlAbstractFontMapShader implements GlShader {

	/**
	 * TAG log
	 */
	@SuppressWarnings("unused")
	private final static String TAG = GlAbstractFontMapShader.class.getName();
	
	/**
	 * Location of ShadowMap vertex shader
	 */
	private final static String VERTEX_SHADER = "/fr/kesk/libgl/shader/gsgl/map/font_map.vtx";
	
	/**
	 * Location of ShadowMap fragment shader
	 */
	private final static String FRAGMENT_SHADER = "/fr/kesk/libgl/shader/gsgl/map/font_map.fgt";
	
	/**
	 * Texture border size matching quality
	 */
	private int quality = QUALITY_MEDIUM;
	
	/**
	 * The associated program for shadow generation
	 */
	private static GlProgram program;
	
	/**
	 * Reference to the current font texture
	 */
	private GlTexture fontTexture;
	
	/**
	 * The map size
	 */
	private int size;	
	
	/**
	 * Map containing fonts
	 */
	private Map<String, Font> fontMap = new HashMap<String, Font>();
	
	/**
	 * The current font style
	 */
	private String style;
	
	/**
	 * The current depth [0,1]
	 */
	private float depth = 0.0f;
	
	/**
	 * Viewport coordinates and size based on GL coordinates [-1,1]
	 */
	private final float[] textViewport = new float[]{-1.0f, 1.0f, 2.0f, 2.0f, 0.1f, 0.1f, 10f, 10f};
	
	/**
	 * Contains the text to display
	 */
	private char[] text = new char[0];
	
	/**
	 * Indicates to update the viewport
	 */
	private boolean updateViewport = true;
	
	/**
	 * Indicates to update the text
	 */
	private boolean updateText = true;
	
	/**
	 * Contains the vertices
	 */
	private FloatBuffer mapBuffer;
	
	/**
	 * Contains the UV map
	 */
	private FloatBuffer uvBuffer;
		
	/**
	 * VBO handle on mapBuffer
	 */
	private int mapBufferHandle = GLES20.GL_FALSE;
	
	/**
	 * VBO handle on uvBuffer
	 */
	private int uvBufferHandle = GLES20.GL_FALSE;
	
	/**
	 * Handle on position in shader
	 */
	private static int a_PositionVec4Handle;
	
	/**
	 * Handle on text coord in shader
	 */
	private static int a_TexCoordinateVec2Handle;
	
	/**
	 * Handle on sampler in shader
	 */
	private static int u_TextureIndexSampler2D;
	
	/**
	 * Gets an input stream on font map image
	 * 
	 * @return An input stream on font map image
	 */
	protected abstract InputStream getFontMapInputStream();
	
	/**
	 * Gets an input stream on font map metadata<br/>
	 * <br/>
	 * Format is based on Java {@link Properties} :
	 * <ul>
	 * <li><b>map.size: </b>image border size (square image only)</li>
	 * <li><b>font.size: </b>size of a character in the font</li>
	 * <li><b>font.data.types: </b>list of font types in comma separated list</li>
	 * <li><b>font.data.${type}.offset: </b>starting offset for ${type} in image</li>
	 * <li><b>font.data.${type}.content: </b>list of characters for current ${type}</li>
	 * </ul>
	 * 
	 * @return An input stream on font map metadata
	 */
	protected abstract InputStream getFontMetadataInputStream();
	
	/**
	 * Configure the text area size, font size and placement based on GL coordinates [-1,1]
	 * 
	 * @param x The top left corner X value
	 * @param y The top left corner Y value
	 * @param col The number of columns
	 * @param row The number of rows
	 * @param fontSize The size of the font relative to view port
	 * @param ratio The display port ratio
	 */
	public void setTextViewport(final float x, final float y, final int col, final int row, final float fontSize, final float ratio){
		//android.util.Log.d(TAG,"setTextViewport("+x+","+y+","+width+","+height+"+","+ratio+")");
		this.textViewport[0] = x; // X 
		this.textViewport[1] = y; // Y 
		this.textViewport[2] = col * fontSize; // width
		this.textViewport[3] = row * fontSize * ratio; // height
		this.textViewport[4] = fontSize; // font width
		this.textViewport[5] = fontSize * ratio; //font height
		this.textViewport[6] = col; // elements in col
		this.textViewport[7] = row; // elements in row
		this.updateViewport = true;
	}
	
	/**
	 * Set the text to display, this method must be overridden for specific layout (carriage return, drawing ...)
	 * 
	 * @param text The text to display 
	 */
	public void setText(final String text){
		//android.util.Log.d(TAG,"setText("+text+")");
		this.text = text.toCharArray();
		this.updateText = true;
	}
	
	/**
	 * @return the style
	 */
	public String getStyle() {
		return style;
	}

	/**
	 * @param style the style to set
	 */
	public void setStyle(String style) {
		this.style = style;
	}
	
	/**
	 * @return the depth
	 */
	public float getDepth() {
		return depth;
	}

	/**
	 * @param depth the depth to set
	 */
	public void setDepth(float depth) {
		this.depth = depth;
	}

	/* (non-Javadoc)
	 * @see fr.kesk.libgl.shader.GlShader#compile()
	 */
	@Override
	public void compile() {
		//android.util.Log.d(TAG,"initialize()");
		//Build program if needed
		if(GlAbstractFontMapShader.program == null){
			InputStream vertIn = null;
			InputStream fragIn = null;
			final InputStream mapIn = this.getFontMapInputStream();
			final InputStream mapDataIn = this.getFontMetadataInputStream();
			try{
				vertIn = this.getClass().getResourceAsStream(VERTEX_SHADER);
				fragIn = this.getClass().getResourceAsStream(FRAGMENT_SHADER);
				GlAbstractFontMapShader.program = new GlProgram(vertIn, fragIn);
				GlAbstractFontMapShader.program.start();
				
				//Uniforms
				GlAbstractFontMapShader.u_TextureIndexSampler2D = GlAbstractFontMapShader.program.getUniformHandle("u_TextureIndexSampler2D");
				
				//Attributes
				GlAbstractFontMapShader.a_PositionVec4Handle = GlAbstractFontMapShader.program.getAttributeHandle("a_PositionVec4");
				GlAbstractFontMapShader.a_TexCoordinateVec2Handle = GlAbstractFontMapShader.program.getAttributeHandle("a_TexCoordinateVec2");
				
				//Get font metadata
				final Properties props = new Properties();
				props.load(mapDataIn);
				
				//Map size 
				if(props.containsKey("map.size")){
					this.size = Integer.parseInt(props.getProperty("map.size"));
				}
				else throw new GLException(GLES20.GL_INVALID_VALUE,"missing map.size in font map data");
				
				//Font size
				final int fontSize;
				if(props.containsKey("font.size")){
					fontSize = Integer.parseInt(props.getProperty("font.size"));
					if(this.size % fontSize != 0){
						throw new GLException(GLES20.GL_INVALID_VALUE,"Texture size must be a multiple of font size");
					}
				}
				else throw new GLException(GLES20.GL_INVALID_VALUE,"missing font.size in font map data");
				
				//Font size
				final String types;
				if(props.containsKey("font.data.types")){
					types = props.getProperty("font.data.types");
				}
				else throw new GLException(GLES20.GL_INVALID_VALUE,"missing font.size in font map data");
				
				//Parse all types
				final StringTokenizer tokenizer = new StringTokenizer(types, ",");
				while(tokenizer.hasMoreTokens()){
					final String name = tokenizer.nextToken();
					final int offset;
					final String characters;
					if(props.containsKey("font.data."+name+".offset")){
						offset = Integer.parseInt(props.getProperty("font.data."+name+".offset"));
					}
					else throw new GLException(GLES20.GL_INVALID_VALUE,"missing font.data."+name+".offset in font map data");
					if(props.containsKey("font.data."+name+".content")){
						characters = props.getProperty("font.data."+name+".content");
					}
					else throw new GLException(GLES20.GL_INVALID_VALUE,"missing font.data."+name+".content in font map data");
					
					this.fontMap.put(name, new Font(fontSize, name, offset, characters));
				}
				
				//Get font image
				this.fontTexture = new GlTexture() {
					
					@Override
					public int getWidth() {
						return GlAbstractFontMapShader.this.size;
					}
					
					@Override
					public int getSize() {
						return GlAbstractFontMapShader.this.size * GlAbstractFontMapShader.this.size * SIZEOF_UNSIGNED_BYTE;
					}
					
					@Override
					public int getType() {
						return GLES20.GL_UNSIGNED_BYTE;
					}

					@Override
					public int getId() {
						return this.hashCode();
					}
					
					@Override
					public int getHeight() {
						return GlAbstractFontMapShader.this.size;
					}
					
					@Override
					public ByteBuffer getBytes() {
						final Bitmap bitmap = BitmapFactory.decodeStream(mapIn);
						final ByteBuffer buffer = (ByteBuffer)ByteBuffer.allocateDirect(this.getSize()).order(ByteOrder.nativeOrder());
						bitmap.copyPixelsToBuffer(buffer);
						bitmap.recycle();
						return (ByteBuffer)buffer.position(0);
					}

					@Override
					public int getWrapMode(int axeId) {
						return GLES20.GL_CLAMP_TO_EDGE;
					}

					@Override
					public int getMagnificationFilter() {
						if(GlAbstractFontMapShader.this.quality > QUALITY_LOW){
							return GLES20.GL_LINEAR;
						}
						else{
							return GLES20.GL_NEAREST;
						}
					}

					@Override
					public int getMinificationFilter() {
						switch(GlAbstractFontMapShader.this.quality){
							case QUALITY_VERY_LOW:
								return GLES20.GL_NEAREST;
							case QUALITY_LOW:
								return GLES20.GL_LINEAR;
							case QUALITY_HIGH:
								return GLES20.GL_LINEAR_MIPMAP_NEAREST;
							case QUALITY_VERY_HIGH:
								return GLES20.GL_LINEAR_MIPMAP_LINEAR;
							default : 
								return GLES20.GL_NEAREST_MIPMAP_LINEAR;
						}
						
					}
				};
				
				final int[] textureHandles = new int[1];
				GLES20.glGenTextures(textureHandles.length, textureHandles, 0);
				this.fontTexture.handle = textureHandles[0];
				GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, this.fontTexture.handle);
				GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, this.fontTexture.getMinificationFilter());
				GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, this.fontTexture.getMagnificationFilter());
				GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
				GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);
				GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, this.fontTexture.getFormat(), this.fontTexture.getWidth(), this.fontTexture.getHeight(), 0, this.fontTexture.getFormat(), this.fontTexture.getType(), this.fontTexture.getBytes());
				GLES20.glGenerateMipmap(GLES20.GL_TEXTURE_2D);				
				
				final int[] handles = new int[2];
				GLES20.glGenBuffers(2, handles, 0);
				this.mapBufferHandle = handles[0];
				this.uvBufferHandle = handles[1];
				
				GlContext.glCheckError();
				
			}catch(IOException ioe){
				throw new GLException(GLES20.GL_INVALID_VALUE,"Failed to load font map : "+ioe.getMessage());
			}finally{
				if(vertIn != null){
					try{
						vertIn.close();
					}catch(IOException ioe){}
				}
				if(fragIn != null){
					try{
						fragIn.close();
					}catch(IOException ioe){}
				}
				if(mapIn != null){
					try{
						mapIn.close();
					}catch(IOException ioe){}
				}
				if(mapDataIn != null){
					try{
						mapDataIn.close();
					}catch(IOException ioe){}
				}
			}
		}
		
		this.updateViewport = true;
		this.updateText = true;
	}

	/* (non-Javadoc)
	 * @see fr.kesk.libgl.shader.GlShader#setQuality(int)
	 */
	@Override
	public void setQuality(int quality) {
		//android.util.Log.d(TAG,"setQuality("+quality+")");
		this.quality = quality;
	}

	/* (non-Javadoc)
	 * @see fr.kesk.libgl.shader.GlShader#render(fr.kesk.libgl.GlAssets.Node)
	 */
	@Override
	public void render(Node nodeInstance) {
		this.render();
	}
	
	/* (non-Javadoc)
	 * @see fr.kesk.libgl.shader.GlShader#render()
	 */
	@Override
	public void render() {
		//android.util.Log.d(TAG,"render()");
		
		//View port has changed, update the mesh and indexes
		if(this.updateViewport){
			if(this.mapBuffer != null){
				ByteBufferPool.getInstance().returnDirectBuffer(this.mapBuffer);
			}
			
			this.mapBuffer = ByteBufferPool.getInstance().getDirectFloatBuffer((int)(this.textViewport[6] * this.textViewport[7] * 18)); // 18 -> 6 vertex * 3 bytes
			final float[] currentCoord = new float[]{this.textViewport[0], this.textViewport[1], this.depth};
			for(int rowIndex=0; rowIndex < this.textViewport[7]; rowIndex++){
				for(int colIndex=0; colIndex < this.textViewport[6]; colIndex++){
					//top-left corner
					currentCoord[0] = this.textViewport[0] + (colIndex * this.textViewport[4]);
					currentCoord[1] = this.textViewport[1] - (rowIndex * this.textViewport[5]);
					this.mapBuffer.put(currentCoord);
					//bottom-left corner
					currentCoord[1] = this.textViewport[1] - ((rowIndex + 1) * this.textViewport[5]);
					this.mapBuffer.put(currentCoord);
					//bottom-right corner
					currentCoord[0] = this.textViewport[0] + ((colIndex + 1) * this.textViewport[4]);
					this.mapBuffer.put(currentCoord);
					//bottom-right corner
					this.mapBuffer.put(currentCoord);
					//top-right corner
					currentCoord[1] = this.textViewport[1] - (rowIndex * this.textViewport[5]);
					this.mapBuffer.put(currentCoord);
					//top-left corner
					currentCoord[0] = this.textViewport[0] + (colIndex * this.textViewport[4]);
					this.mapBuffer.put(currentCoord);
				}
			}
			
			
			GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, this.mapBufferHandle);
			this.mapBuffer.position(0);
			GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, this.mapBuffer.capacity() * GlBuffer.SIZEOF_JAVA_FLOAT, this.mapBuffer, GLES20.GL_DYNAMIC_DRAW);

			if(this.uvBuffer != null){
				ByteBufferPool.getInstance().returnDirectBuffer(this.uvBuffer);
				this.uvBuffer = null;
			}
			
			this.updateText = true;
		}

		//View port has changed, update the mesh and texcoord
		if(this.updateText){
			if(this.uvBuffer == null){
				this.uvBuffer = ByteBufferPool.getInstance().getDirectFloatBuffer((int)(this.textViewport[6] * this.textViewport[7] * 12)); // 12 -> 6 vertex * 2 bytes
			}
			else{
				this.uvBuffer.clear();
			}
			
			final Font font = this.fontMap.get(this.style);
			final float sourceFontSize = (float)font.size / (float)this.size;
			final int charPerLine = this.size / font.size;
			for(int charIndex=0; charIndex < this.text.length; charIndex++){
				if(this.text[charIndex] == '\n'){
					final int pos = this.uvBuffer.position();
					final int leftQuads = ((int)this.textViewport[7]*12) - (pos % ((int)this.textViewport[7]*12));
					this.uvBuffer.position(pos + leftQuads);
				}
				else{
					final int offset = font.offset + font.data.get(this.text[charIndex]);
					final float sourceRow = (offset / charPerLine) * sourceFontSize;
					final float sourceCol = (offset % charPerLine) * sourceFontSize;
					//top-left corner
					this.uvBuffer.put(sourceCol);
					this.uvBuffer.put(sourceRow);
					//bottom-left corner
					this.uvBuffer.put(sourceCol);
					this.uvBuffer.put(sourceRow + sourceFontSize);
					//bottom-right corner
					this.uvBuffer.put(sourceCol + sourceFontSize);
					this.uvBuffer.put(sourceRow + sourceFontSize);
					//bottom-right corner
					this.uvBuffer.put(sourceCol + sourceFontSize);
					this.uvBuffer.put(sourceRow + sourceFontSize);
					//top-right corner
					this.uvBuffer.put(sourceCol + sourceFontSize);
					this.uvBuffer.put(sourceRow);
					//top-left corner
					this.uvBuffer.put(sourceCol);
					this.uvBuffer.put(sourceRow);				
				}	
			}
			
			GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, this.uvBufferHandle);
			this.uvBuffer.position(0);
			if(this.updateViewport){
				GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, this.uvBuffer.capacity() * GlBuffer.SIZEOF_JAVA_FLOAT, this.uvBuffer, GLES20.GL_DYNAMIC_DRAW);
			}
			else{
				GLES20.glBufferSubData(GLES20.GL_ARRAY_BUFFER, 0, this.uvBuffer.capacity() * GlBuffer.SIZEOF_JAVA_FLOAT, this.uvBuffer);
			}
			
			this.updateViewport = false;
			this.updateText = false;
		}
		
		GlAbstractFontMapShader.program.start();

		GLES20.glEnableVertexAttribArray(GlAbstractFontMapShader.a_PositionVec4Handle);
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, this.mapBufferHandle);
		GLES20Utils.glVertexAttribPointer(GlAbstractFontMapShader.a_PositionVec4Handle, 3, GLES20.GL_FLOAT, false, 0, 0);
		
		GLES20.glEnableVertexAttribArray(GlAbstractFontMapShader.a_TexCoordinateVec2Handle);
		GLES20.glUniform1f(GlAbstractFontMapShader.u_TextureIndexSampler2D, 0);
		GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, this.fontTexture.handle);
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, this.uvBufferHandle);
		GLES20Utils.glVertexAttribPointer(GlAbstractFontMapShader.a_TexCoordinateVec2Handle, 2, GLES20.GL_FLOAT, false, 0, 0);
		
		GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, (int)(this.textViewport[6] * this.textViewport[7] * 6));
		
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, GlTexture.UNBIND_HANDLE);
		GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, GlBuffer.UNBIND_HANDLE);
		GLES20.glDisableVertexAttribArray(GlAbstractFontMapShader.a_PositionVec4Handle);
		GLES20.glDisableVertexAttribArray(GlAbstractFontMapShader.a_TexCoordinateVec2Handle);
	}

	/* (non-Javadoc)
	 * @see fr.kesk.libgl.shader.GlShader#free()
	 */
	@Override
	public void free() {
		//android.util.Log.d(TAG,"free()");
		if(GlAbstractFontMapShader.program != null){
			GlAbstractFontMapShader.program.free();
			GlAbstractFontMapShader.program = null;
		}
		if(this.fontTexture != null){
			this.fontTexture.free();
			this.fontTexture = null;
		}
		if(this.mapBuffer != null){
			ByteBufferPool.getInstance().returnDirectBuffer(this.mapBuffer);
			ByteBufferPool.getInstance().returnDirectBuffer(this.uvBuffer);
			GLES20.glDeleteBuffers(2, new int[]{this.mapBufferHandle, this.uvBufferHandle}, 0);
			this.mapBuffer = null;
			this.uvBuffer = null;
		}
	}
	
	/**
	 * Font abstraction class for mapping characters and map entries
	 * 
	 * @author Thomas MILLET
	 *
	 */
	public static final class Font{
		
		/**
		 * The font size
		 */
		public final int size;
		
		/**
		 * The font name
		 */
		public final String name;
		
		/**
		 * The font offset in map
		 */
		public final int offset;
		
		/**
		 * The font data
		 */
		public final SparseIntArray data;
		
		/**
		 * Default constructor
		 * 
		 * @param size The font size
		 * @param name The font name
		 * @param offset The font offset in map
		 * @param characters The raw characters list
		 */
		public Font(final int size, final String name, final int offset, final CharSequence characters){
			//android.util.Log.d(TAG,"Font.NEW");
			this.size = size;
			this.name = name;
			this.offset = offset;
			this.data = new SparseIntArray();
			this.buildData(characters);
		}
		
		/**
		 * Fill data using font configuration
		 */
		private void buildData(final CharSequence characters){
			//android.util.Log.d(TAG,"Font.buildData()");
			final int length = characters.length();
			for(int i=0; i < length ; i++){
				this.data.put(characters.charAt(i), i);
			}
		}
	}

}
