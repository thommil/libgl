package fr.kesk.libgl.tools.filter;

import fr.kesk.libgl.tools.minifier.collada.COLLADA;

/**
 * Filter to transform COLLADA document in another one
 * 
 * @author Thomas MILLET
 *
 */
public interface COLLADAFilter {

	public COLLADA parse(final COLLADA colladaSource);

}
