package fr.kesk.libgl.tools.filter;

import java.util.ArrayList;

import fr.kesk.libgl.tools.minifier.collada.COLLADA;

/**
 * Simple container to handle an ordered list of COLLADAFilter
 * 
 * @author Thomas MILLET
 *
 */
public class COLLADAFilterList extends ArrayList<COLLADAFilter> implements COLLADAFilter {

	/* (non-Javadoc)
	 * @see fr.kesk.libgl.tools.filter.COLLADAFilter#parse(fr.kesk.libgl.tools.minifier.collada.COLLADA)
	 */
	@Override
	public COLLADA parse(COLLADA colladaSource) {
		for(COLLADAFilter filter : this){
			System.out.println("        > Applying filter : "+filter.getClass().getSimpleName());
			colladaSource = filter.parse(colladaSource);
		}
		return colladaSource;
	}
}
