package fr.kesk.libgl.tools.minifier;

import java.awt.image.BufferedImage;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.Channels;
import java.nio.channels.WritableByteChannel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import fr.kesk.libgl.loader.API;
import fr.kesk.libgl.tools.filter.COLLADAFilter;
import fr.kesk.libgl.tools.minifier.collada.COLLADA;
import fr.kesk.libgl.tools.minifier.collada.Camera;
import fr.kesk.libgl.tools.minifier.collada.CommonNewparamType;
import fr.kesk.libgl.tools.minifier.collada.Effect;
import fr.kesk.libgl.tools.minifier.collada.Extra;
import fr.kesk.libgl.tools.minifier.collada.FxSampler2DCommon;
import fr.kesk.libgl.tools.minifier.collada.Geometry;
import fr.kesk.libgl.tools.minifier.collada.Image;
import fr.kesk.libgl.tools.minifier.collada.InputLocalOffset;
import fr.kesk.libgl.tools.minifier.collada.InstanceEffect;
import fr.kesk.libgl.tools.minifier.collada.InstanceGeometry;
import fr.kesk.libgl.tools.minifier.collada.InstanceMaterial;
import fr.kesk.libgl.tools.minifier.collada.InstanceWithExtra;
import fr.kesk.libgl.tools.minifier.collada.LibraryCameras;
import fr.kesk.libgl.tools.minifier.collada.LibraryEffects;
import fr.kesk.libgl.tools.minifier.collada.LibraryGeometries;
import fr.kesk.libgl.tools.minifier.collada.LibraryImages;
import fr.kesk.libgl.tools.minifier.collada.LibraryLights;
import fr.kesk.libgl.tools.minifier.collada.LibraryMaterials;
import fr.kesk.libgl.tools.minifier.collada.LibraryNodes;
import fr.kesk.libgl.tools.minifier.collada.LibraryVisualScenes;
import fr.kesk.libgl.tools.minifier.collada.Light;
import fr.kesk.libgl.tools.minifier.collada.Lines;
import fr.kesk.libgl.tools.minifier.collada.Linestrips;
import fr.kesk.libgl.tools.minifier.collada.Lookat;
import fr.kesk.libgl.tools.minifier.collada.Material;
import fr.kesk.libgl.tools.minifier.collada.Matrix;
import fr.kesk.libgl.tools.minifier.collada.Node;
import fr.kesk.libgl.tools.minifier.collada.NodeType;
import fr.kesk.libgl.tools.minifier.collada.Param;
import fr.kesk.libgl.tools.minifier.collada.Polylist;
import fr.kesk.libgl.tools.minifier.collada.ProfileCOMMON;
import fr.kesk.libgl.tools.minifier.collada.Rotate;
import fr.kesk.libgl.tools.minifier.collada.Skew;
import fr.kesk.libgl.tools.minifier.collada.Source;
import fr.kesk.libgl.tools.minifier.collada.TargetableFloat;
import fr.kesk.libgl.tools.minifier.collada.TargetableFloat3;
import fr.kesk.libgl.tools.minifier.collada.Technique;
import fr.kesk.libgl.tools.minifier.collada.Triangles;
import fr.kesk.libgl.tools.minifier.collada.Trifans;
import fr.kesk.libgl.tools.minifier.collada.Tristrips;
import fr.kesk.libgl.tools.minifier.collada.VisualScene;
import fr.kesk.libgl.tools.minifier.collada.InstanceMaterial.BindVertexInput;



/**
 * Create LibGL input files based on COLLADA
 * 
 * @author Thomas MILLET
 *
 */
public class COLLADAMinifier {

	/**
	 * Mode verbose
	 */
	public static boolean MODE_VERBOSE = false;
	
	/**
	 * Indicates the output folder in which files will be put
	 */
	private static final String OUT_FOLDER = "./OUT";
		
	/**
	 * Default Light values
	 */
	private static final float DEFAULT_LIGHT_KC = 1.0f;
	private static final float DEFAULT_LIGHT_KL = 0.0f;
	private static final float DEFAULT_LIGHT_KQ = 0.0f;
	private static final float DEFAULT_LIGHT_FALLOFF_ANGLE = 180.0f;
	private static final float DEFAULT_LIGHT_FALLOFF_EXP = 0.0f;
	
	/**
	 * Default Camera values
	 */
	private static final float DEFAULT_PERSPECTIVE_XFOV = 45f;
	private static final float DEFAULT_PERSPECTIVE_YFOV = 45f;
	private static final float DEFAULT_PERSPECTIVE_ZNEAR = 0.1f;
	private static final float DEFAULT_PERSPECTIVE_ZFAR = 100.0f;
	
	private static final float DEFAULT_ORTHO_XMAG = 1.0f;
	private static final float DEFAULT_ORTHO_YMAG = 1.0f;
	private static final float DEFAULT_ORTHO_ZNEAR = 0.1f;
	private static final float DEFAULT_ORTHO_ZFAR = 100.0f;
	
	/**
	 * Get the targeted ID from a specified URI
	 * 
	 * @param uri The target URI
	 * @return The targeted ID
	 */
	public static final int URIToID(String uri){
		return uri.replaceAll("#", "").hashCode();
	}
	
	/**
	 * Transform a XML id in an Java const variable name;
	 * 
	 * @param id The XML id
	 * @return The const name
	 */
	public static final String IDtoConst(String id){
		return id.replaceAll("#", "").replaceAll("[^a-zA-Z0-9]", "_").toUpperCase();
	}
	
	/**
	 * Parse nodes and store it in the flatten list
	 * 
	 * @param rootNode The root node to parse
	 * @param nodes The global nodes list
	 */
	public static void flattenNodes(final COLLADA collada, final Node rootNode, List<Node> nodes){
		for(InstanceWithExtra nodeInstance : rootNode.getInstanceNode()){
			int nodeInstanceId = URIToID(nodeInstance.getUrl());
			for(Object element : collada.getLibraryAnimationsOrLibraryAnimationClipsOrLibraryCameras()){
				if(element instanceof LibraryNodes){
					for(Node node : ((LibraryNodes)element).getNode()){
						if(node.getId().hashCode() == nodeInstanceId){
							flattenNodes(collada, node, nodes);
							break;
						}
					}
				}
			}
		}
		
		for(Node node : rootNode.getNode()){
			flattenNodes(collada, node, nodes);
		}
		if(!nodes.contains(rootNode)) nodes.add(rootNode);
	}
	
	/**
	 * Parse and generate ouput files for geometry
	 * 
	 * @param collada The COLLADA instance
	 * @param inFile The input file
	 * @param outStream The output stream
	 */
	@SuppressWarnings({ "rawtypes"})
	public static void parse(final COLLADA collada, final File inFile, final OutputStream outStream) throws IOException{
		
		String packageName = null;
		String apiClassname = null;
		int geometryCount = 0;
		int lightCount = 0;
		int cameraCount = 0;
		int imageCount = 0;
		int materialCount = 0;
		int nodeCount = 0;
		int sceneCount = 0;
		
		//Search API ID package name
		for(Extra extra : collada.getExtra()){
			for(Technique technique : extra.getTechnique()){
				if(technique.getProfile().equals("lgl")){
					for(Object object : technique.getAny()){
						if(object instanceof Param && ((Param)object).getName().equals("package")){
							packageName = ((Param)object).getValue().trim();
						}
						else if(object instanceof Param && ((Param)object).getName().equals("classname")){
							apiClassname = ((Param)object).getValue().trim();
						}
					}
				}
			}
		}
		
		//Build API ID filename
		if(apiClassname == null){
			apiClassname = inFile.getName().substring(0, 1).toUpperCase() + inFile.getName().substring(1);
			apiClassname = apiClassname.replaceAll("^([^\\.]*).*$", "$1").replaceAll("[^a-zA-Z0-9]", "_");
		}	
		
		if(MODE_VERBOSE) System.out.println("    > Metadata");
		if(MODE_VERBOSE) System.out.println("        > package : "+ (packageName == null? "default":packageName));
		if(MODE_VERBOSE) System.out.println("        > API class : "+apiClassname);
		
		DataOutputStream fileOut = null;
		FileWriter apiWriter = null;
		try{
			int errorCount = 0;
			int warningCount = 0;
			fileOut = new DataOutputStream(outStream);
			apiWriter = new FileWriter(OUT_FOLDER+"/"+apiClassname+".java");
		
			//Init API file
			if(packageName != null){
				apiWriter.write("package "+packageName+";\n\n");
			}
			apiWriter.write("public interface "+apiClassname+"{\n");
			
			//Search for library effects for futur search
			LibraryEffects libraryEffects = null;
			for(Object element : collada.getLibraryAnimationsOrLibraryAnimationClipsOrLibraryCameras()){
				if(element instanceof LibraryEffects){
					libraryEffects = (LibraryEffects)element;
				}
			}
			
			//Store nodes for flat treatment
			List<Node> allNodes = new ArrayList<Node>();
			
			/************************************************************************************************
			 * HEADER - Header and metadata	
			 ************************************************************************************************
			 
			  [HEADER][$VERSION]
			  [$GEOMETRY_COUNT][$LIGHT_COUNT][$CAMERA_COUNT][$IMAGERY_COUNT][$MATERIAL_COUNT][$SCENE_COUNT]
			  
			 ************************************************************************************************/
			if(MODE_VERBOSE) System.out.println("    > Header");
			fileOut.writeInt(API.HEADER);
			fileOut.writeFloat(API.VERSION);
			if(MODE_VERBOSE) System.out.println("        > Version : "+API.VERSION);
			
			for(Object element : collada.getLibraryAnimationsOrLibraryAnimationClipsOrLibraryCameras()){
				if(element instanceof LibraryGeometries){
					geometryCount = ((LibraryGeometries)element).getGeometry().size();
				}
				else if(element instanceof LibraryLights){
					lightCount = ((LibraryLights)element).getLight().size();
				}
				else if(element instanceof LibraryCameras){
					cameraCount = ((LibraryCameras)element).getCamera().size();
				}
				else if(element instanceof LibraryImages){
					imageCount = ((LibraryImages)element).getImage().size();
				}
			}
			
			for(Object element : collada.getLibraryAnimationsOrLibraryAnimationClipsOrLibraryCameras()){
				if(element instanceof LibraryNodes){
					for(Node node : ((LibraryNodes)element).getNode()){
						flattenNodes(collada, node, allNodes);
					}
				}
			}
			
			for(Object element : collada.getLibraryAnimationsOrLibraryAnimationClipsOrLibraryCameras()){
				if(element instanceof LibraryVisualScenes){
					sceneCount = ((LibraryVisualScenes)element).getVisualScene().size();
					for(VisualScene scene : ((LibraryVisualScenes)element).getVisualScene()){
						for(Node node : scene.getNode()){
							flattenNodes(collada, node, allNodes);
						}
					}
				}
			}
			nodeCount = allNodes.size();
			
			fileOut.writeInt(geometryCount);
			fileOut.writeInt(lightCount);
			fileOut.writeInt(cameraCount);
			fileOut.writeInt(imageCount);
			fileOut.writeInt(materialCount);
			fileOut.writeInt(nodeCount);
			fileOut.writeInt(sceneCount);

			System.out.println("    > Geometries : "+geometryCount);
			System.out.println("    > Lights : "+lightCount);
			System.out.println("    > Cameras : "+cameraCount);
			System.out.println("    > Images : "+imageCount);
			System.out.println("    > Materials : "+materialCount);
			System.out.println("    > Nodes : "+nodeCount);
			System.out.println("    > Scenes : "+sceneCount);
			
			geometryCount = lightCount = cameraCount = imageCount = materialCount = nodeCount = sceneCount = 0;
			
			for(Object element : collada.getLibraryAnimationsOrLibraryAnimationClipsOrLibraryCameras()){
				/************************************************************************************************
				 * GEOMETRY	- Describes the visual shape and appearance of an object in a scene.	
				 ************************************************************************************************
				 
				  [GEOMETRY][$ID][$PRIMITIVES_COUNT]
				  [$SEMANTIC][$SET][$SIZE][$OFFSET]...[$SEMANTIC][$SET][$SIZE][$OFFSET]
				  [$SEMANTIC][$SIZE][$OFFSET][$SET]...[$SEMANTIC][$SIZE][$OFFSET][$SET]
				  [$VALUES]
				  ...
				  [$PRIMITIVE_TYPE][$INPUT_COUNT][$VERTEX_COUNT][$MATERIAL_ID]
				  [$SEMANTIC][$SET][$SIZE][$OFFSET]...[$SEMANTIC][$SET][$SIZE][$OFFSET]
				  [$VALUES]
				  ...
				  [GEOMETRY][$ID][$PRIMITIVES_COUNT]
				  
				 ************************************************************************************************/
				if(element instanceof LibraryGeometries){
					if(MODE_VERBOSE) System.out.println("    > Geometry Elements");
					apiWriter.write("\n\t//LibraryGeometries\n");
					for(Geometry geometry : ((LibraryGeometries)element).getGeometry()){
						if(MODE_VERBOSE) System.out.println("            > Parsing "+geometry.getId());
						int ID = geometry.getId().hashCode();
						int PRIMITIVES_COUNT = geometry.getMesh().getLinesOrLinestripsOrPolygons().size();
						
						if(MODE_VERBOSE) System.out.println("                > ID : "+ID);
						if(MODE_VERBOSE) System.out.println("                > PRIMITIVES_COUNT : "+PRIMITIVES_COUNT);
						apiWriter.write("\tpublic static final int GEOMETRY_"+IDtoConst(geometry.getId())+" = "+ID+";\n");
						
						fileOut.writeInt(API.GEOMETRY);
						fileOut.writeInt(ID);
						fileOut.writeInt(PRIMITIVES_COUNT);
						
						//Parse primitives
						for(Object primitiveElement : geometry.getMesh().getLinesOrLinestripsOrPolygons()){
							int PRIMITIVE_TYPE = API.EMPTY;
							int INPUT_COUNT = 0;
							int VERTEX_COUNT = 0;
							int MATERIAL_ID=0;
							float[] VALUES = null;
							List<BigInteger> inputPValues = null;
							int valuesCount = 0;
							final List<InputLocalOffset> inputs;
							if(primitiveElement instanceof Polylist){
								if(MODE_VERBOSE) System.out.println("                    > Primitive POLYLIST");
								PRIMITIVE_TYPE = API.POLYLIST;
								inputs = ((Polylist)primitiveElement).getInput();
								inputPValues = ((Polylist)primitiveElement).getP();
								boolean notSupported = false;
								for(BigInteger v : ((Polylist)primitiveElement).getVcount()){
									if(v.intValue() == 3){
										VERTEX_COUNT += v.intValue();
									}
									else{
										notSupported = true;
										break;
									}
								}
								if(notSupported){
									System.err.println("                        [ERROR] Unsupported POLYLIST, only triangles are accepted");
									errorCount++;
									continue;
								}
								if(((Polylist)primitiveElement).getMaterial() != null) MATERIAL_ID = URIToID(((Polylist)primitiveElement).getMaterial());
							}
							else if(primitiveElement instanceof Lines){
								if(MODE_VERBOSE) System.out.println("                    > Primitive LINES");
								PRIMITIVE_TYPE = API.LINES;
								inputs = ((Lines)primitiveElement).getInput();
								inputPValues = ((Lines)primitiveElement).getP();
								VERTEX_COUNT = ((Lines)primitiveElement).getCount().intValue() * 2;
								if(((Lines)primitiveElement).getMaterial() != null) MATERIAL_ID = URIToID(((Lines)primitiveElement).getMaterial());
							}
							else if(primitiveElement instanceof Linestrips){
								if(MODE_VERBOSE) System.out.println("                    > Primitive LINESTRIPS");
								PRIMITIVE_TYPE = API.LINESTRIPS;
								inputs = ((Linestrips)primitiveElement).getInput();
								inputPValues = ((Linestrips)primitiveElement).getP();
								VERTEX_COUNT = ((Linestrips)primitiveElement).getCount().intValue() + 1;
								if(((Linestrips)primitiveElement).getMaterial() != null) MATERIAL_ID = URIToID(((Linestrips)primitiveElement).getMaterial());
							}
							else if(primitiveElement instanceof Triangles){
								if(MODE_VERBOSE) System.out.println("                    > Primitive TRIANGLES");
								PRIMITIVE_TYPE = API.TRIANGLES;
								inputs = ((Triangles)primitiveElement).getInput();
								inputPValues = ((Triangles)primitiveElement).getP();
								VERTEX_COUNT = ((Triangles)primitiveElement).getCount().intValue() * 3;
								if(((Triangles)primitiveElement).getMaterial() != null) MATERIAL_ID = URIToID(((Triangles)primitiveElement).getMaterial());
							}
							else if(primitiveElement instanceof Trifans){
								if(MODE_VERBOSE) System.out.println("                    > Primitive TRIFANS");
								PRIMITIVE_TYPE = API.TRIFANS;
								inputs = ((Trifans)primitiveElement).getInput();
								inputPValues = ((Trifans)primitiveElement).getP();
								VERTEX_COUNT = ((Trifans)primitiveElement).getCount().intValue() + 2;
								if(((Trifans)primitiveElement).getMaterial() != null) MATERIAL_ID = URIToID(((Trifans)primitiveElement).getMaterial());
							}
							else if(primitiveElement instanceof Tristrips){
								if(MODE_VERBOSE) System.out.println("                    > Primitive TRISTRIPS");
								PRIMITIVE_TYPE = API.TRISTRIPS;
								inputs = ((Tristrips)primitiveElement).getInput();
								inputPValues = ((Tristrips)primitiveElement).getP();
								VERTEX_COUNT = ((Tristrips)primitiveElement).getCount().intValue() + 2;
								if(((Tristrips)primitiveElement).getMaterial() != null) MATERIAL_ID = URIToID(((Tristrips)primitiveElement).getMaterial());
							}
							else{
								System.err.println("                    [ERROR] Unsupported primitive type found : "+primitiveElement.getClass().getName());
								errorCount++;
								continue;
							}
							
							INPUT_COUNT = inputs.size();
							if(MODE_VERBOSE) System.out.println("                        > INPUT_COUNT : "+INPUT_COUNT);
							if(MODE_VERBOSE) System.out.println("                        > VERTEX_COUNT : "+VERTEX_COUNT);
							if(MODE_VERBOSE) System.out.println("                        > MATERIAL_ID : "+((MATERIAL_ID == 0)? "none":MATERIAL_ID));
							
							Map<String, int[]> inputOrderMap = new HashMap<String, int[]>();
							Map<String, float[]> inputValuesMap = new HashMap<String, float[]>();
							
							//Parse inputs and values
							int currentInputOffset = 0;
							for(InputLocalOffset input : inputs){
								if(MODE_VERBOSE) System.out.println("                        > Input "+input.getSemantic());
								Source inputSource = null;
								String sourceId = null;
								float[] inputValues = null;
								int SET = (input.getSet() == null) ? 0:input.getSet().intValue();
								int SIZE = 0;
								int OFFSET = 0;
								
								if(input.getSemantic().equals("VERTEX")){
									sourceId = geometry.getMesh().getVertices().getInput().get(0).getSource().replace("#", "");
								}
								else{
									sourceId = input.getSource().replace("#", "");
								}
								
								for(Source source : geometry.getMesh().getSource()){
									if(source.getId().equals(sourceId)){
										inputSource = source;
										break;
									}
								}
								
								if(inputSource == null){
									System.err.println("                            [ERROR] Source "+sourceId+" not found");
									errorCount++;
									break;
								}
								
								SIZE = inputSource.getTechniqueCommon().getAccessor().getParam().size();
								OFFSET = currentInputOffset;
								if(MODE_VERBOSE) System.out.println("                            > SET : "+SET);
								if(MODE_VERBOSE) System.out.println("                            > SIZE : "+SIZE);
								if(MODE_VERBOSE) System.out.println("                            > OFFSET : "+OFFSET);
								
								currentInputOffset += SIZE; 
								
								if(inputSource.getFloatArray() != null){
									inputValues = new float[inputSource.getFloatArray().getCount().intValue()];
									int fIndex = 0;
									for(Double value : inputSource.getFloatArray().getValue()){
										inputValues[fIndex++] = value.floatValue();
									}
								}
								else if(inputSource.getIntArray() != null){
									if(MODE_VERBOSE) System.out.println("                            [WARNING] Integer dataflow in source "+sourceId);
									warningCount++;
									inputValues = new float[inputSource.getIntArray().getCount().intValue()];
									int iIndex = 0;
									for(Long value : inputSource.getIntArray().getValue()){
										inputValues[iIndex++] = value.floatValue();
									}
								}
								else if(inputSource.getBoolArray() != null){
									if(MODE_VERBOSE) System.out.println("                            [WARNING] Boolean dataflow in source "+sourceId);
									warningCount++;
									inputValues = new float[inputSource.getBoolArray().getCount().intValue()];
									int bIndex = 0;
									for(Boolean value : inputSource.getBoolArray().getValue()){
										inputValues[bIndex++] = value.booleanValue() ? 1.0f : 0.0f;
									}
								}
								else if(inputSource.getIDREFArray() != null || inputSource.getNameArray() != null){
									System.err.println("                            [ERROR] Unsupported data flow in source "+sourceId);
									errorCount++;
									break;
								}
								
								valuesCount += (SIZE * VERTEX_COUNT);
								inputOrderMap.put(input.getSemantic()+SET, new int[]{SET, SIZE, OFFSET});
								inputValuesMap.put(input.getSemantic()+SET, inputValues);
							}
							
							//Fill values
							VALUES = new float[valuesCount];
							if(MODE_VERBOSE) System.out.println("                        > VALUES : "+VALUES.length);
							int set = 0;
							int maxPOffset = 0;
							int stride = 0;
							for(InputLocalOffset input : inputs){
								set = (input.getSet() == null) ? 0:input.getSet().intValue();  
								if(input.getOffset().intValue() > maxPOffset) maxPOffset = input.getOffset().intValue();
								stride+=inputOrderMap.get(input.getSemantic()+set)[1];
							}
							for(InputLocalOffset input : inputs){
								set = (input.getSet() == null) ? 0:input.getSet().intValue();
								float[] sourceValues = inputValuesMap.get(input.getSemantic()+set);
								int inputSize = inputOrderMap.get(input.getSemantic()+set)[1];
								int inputOffset = inputOrderMap.get(input.getSemantic()+set)[2];
								int inputPOffset = input.getOffset().intValue(); 
								for(int inputPIndex = inputPOffset, vertexIndex=0; inputPIndex < inputPValues.size(); inputPIndex += (maxPOffset + 1), vertexIndex++){
									int inputPValue = inputPValues.get(inputPIndex).intValue();
									for(int vIndex=0; vIndex < inputSize; vIndex++){
										VALUES[((vertexIndex * stride) + inputOffset)+vIndex] = sourceValues[(inputPValue*inputSize)+vIndex];
									}
								}
							}
							
							//Write to stream
							fileOut.writeInt(PRIMITIVE_TYPE);
							fileOut.writeInt(INPUT_COUNT);
							fileOut.writeInt(VERTEX_COUNT);
							fileOut.writeInt(MATERIAL_ID);
							for(InputLocalOffset input : inputs){
								set = (input.getSet() == null) ? 0:input.getSet().intValue();
								if(input.getSemantic().startsWith("BINORMAL")) fileOut.writeInt(API.BINORMAL);
								else if(input.getSemantic().startsWith("COLOR")) fileOut.writeInt(API.COLOR);
								else if(input.getSemantic().startsWith("CONTINUITY")) fileOut.writeInt(API.CONTINUITY);
								else if(input.getSemantic().startsWith("IMAGE")) fileOut.writeInt(API.IMAGE);
								else if(input.getSemantic().startsWith("INPUT")) fileOut.writeInt(API.INPUT);
								else if(input.getSemantic().startsWith("IN_TANGENT")) fileOut.writeInt(API.IN_TANGENT);
								else if(input.getSemantic().startsWith("INTERPOLATION")) fileOut.writeInt(API.INTERPOLATION);
								else if(input.getSemantic().startsWith("INV_BIND_MATRIX")) fileOut.writeInt(API.INV_BIND_MATRIX);
								else if(input.getSemantic().startsWith("JOINT")) fileOut.writeInt(API.JOINT);
								else if(input.getSemantic().startsWith("LINEAR_STEPS")) fileOut.writeInt(API.LINEAR_STEPS);
								else if(input.getSemantic().startsWith("MORPH_TARGET")) fileOut.writeInt(API.MORPH_TARGET);
								else if(input.getSemantic().startsWith("MORPH_WEIGHT")) fileOut.writeInt(API.MORPH_WEIGHT);
								else if(input.getSemantic().startsWith("NORMAL")) fileOut.writeInt(API.NORMAL);
								else if(input.getSemantic().startsWith("OUTPUT")) fileOut.writeInt(API.OUTPUT);
								else if(input.getSemantic().startsWith("OUT_TANGENT")) fileOut.writeInt(API.OUT_TANGENT);
								else if(input.getSemantic().startsWith("POSITION")) fileOut.writeInt(API.POSITION);
								else if(input.getSemantic().startsWith("TANGENT")) fileOut.writeInt(API.TANGENT);
								else if(input.getSemantic().startsWith("TEXBINORMAL")) fileOut.writeInt(API.TEXBINORMAL);
								else if(input.getSemantic().startsWith("TEXCOORD")) fileOut.writeInt(API.TEXCOORD);
								else if(input.getSemantic().startsWith("TEXTANGENT")) fileOut.writeInt(API.TEXTANGENT);
								else if(input.getSemantic().startsWith("UV")) fileOut.writeInt(API.UV);
								else if(input.getSemantic().startsWith("VERTEX")) fileOut.writeInt(API.VERTEX);
								else if(input.getSemantic().startsWith("WEIGHT")) fileOut.writeInt(API.WEIGHT);
								else fileOut.writeInt(API.EMPTY);
								fileOut.writeInt(inputOrderMap.get(input.getSemantic()+set)[0]);
								fileOut.writeInt(inputOrderMap.get(input.getSemantic()+set)[1]);
								fileOut.writeInt(inputOrderMap.get(input.getSemantic()+set)[2]);
								
							}
																					
							ByteBuffer buffer = (ByteBuffer)ByteBuffer.allocateDirect(VALUES.length*4).order(ByteOrder.LITTLE_ENDIAN).clear();
							for(int i=0; i < VALUES.length; i++) {
								buffer.putFloat(VALUES[i]);
							}
							buffer.flip();
							WritableByteChannel channel = Channels.newChannel(outStream);
							channel.write(buffer);

						}
						geometryCount++;
					}
					if(geometryCount == 0){
						if(MODE_VERBOSE) System.out.println("            > No geometry found");
					}
				}
				/************************************************************************************************
				 * LIGHT	- Describes the light source in the scene	
				 ************************************************************************************************
				 
				  [LIGHT][$ID][$LIGHT_TYPE]
				  case AMBIENT : [$COLOR_R][$COLOR_G][$COLOR_B]
				  case DIRECTIONAL : [$COLOR_R][$COLOR_G][$COLOR_B]
				  case POINT : [$COLOR_R][$COLOR_G][$COLOR_B][$KC][$KL][$KQ]
				  case SPOT : [$COLOR_R][$COLOR_G][$COLOR_B][$KC][$KL][$KQ][$FALLOFF_ANGLE][$FALLOFF_EXP]
				  ...
				  [LIGHT][$ID]
				  
				 ************************************************************************************************/
				else if(element instanceof LibraryLights){
					if(MODE_VERBOSE) System.out.println("    > Light Elements");
					apiWriter.write("\n\t//LibraryLights\n");
					for(Light light : ((LibraryLights)element).getLight()){
						if(MODE_VERBOSE) System.out.println("            > Parsing "+light.getId());
						int ID = light.getId().hashCode();

						if(MODE_VERBOSE) System.out.println("                > ID : "+ID);
						apiWriter.write("\tpublic static final int LIGHT_"+IDtoConst(light.getId())+" = "+ID+";\n");
						
						
						if(light.getTechniqueCommon().getAmbient() != null){
							if(MODE_VERBOSE) System.out.println("                > TYPE : ambient");
							if(light.getTechniqueCommon().getAmbient().getColor() != null){
								fileOut.writeInt(API.LIGHT);
								fileOut.writeInt(ID);
								fileOut.writeInt(API.AMBIENT);
								if(MODE_VERBOSE) System.out.print("                > RGB : ");
								for(Double value : light.getTechniqueCommon().getAmbient().getColor().getValue()){
									fileOut.writeFloat(value.floatValue());
									if(MODE_VERBOSE) System.out.print(value+" ");
								}
								if(MODE_VERBOSE) System.out.print("\n");
							}
							else{
								System.err.println("            [ERROR] Missing color element");
								errorCount++;
								continue;
							}
						}
						else if(light.getTechniqueCommon().getDirectional() != null){
							if(MODE_VERBOSE) System.out.println("                > TYPE : directional");
							if(light.getTechniqueCommon().getDirectional().getColor() != null){
								fileOut.writeInt(API.LIGHT);
								fileOut.writeInt(ID);
								fileOut.writeInt(API.DIRECTIONAL);
								if(MODE_VERBOSE) System.out.print("                > RGB : ");
								for(Double value : light.getTechniqueCommon().getDirectional().getColor().getValue()){
									fileOut.writeFloat(value.floatValue());
									if(MODE_VERBOSE) System.out.print(value+" ");
								}
								if(MODE_VERBOSE) System.out.print("\n");
							}
							else{
								System.err.println("            [ERROR] Missing color element");
								errorCount++;
								continue;
							}
						}
						else if(light.getTechniqueCommon().getPoint() != null){
							if(MODE_VERBOSE) System.out.println("                > TYPE : point");
							if(light.getTechniqueCommon().getPoint().getColor() != null){
								fileOut.writeInt(API.LIGHT);
								fileOut.writeInt(ID);
								fileOut.writeInt(API.POINT);
								if(MODE_VERBOSE) System.out.print("                > RGB : ");
								for(Double value : light.getTechniqueCommon().getPoint().getColor().getValue()){
									fileOut.writeFloat(value.floatValue());
									if(MODE_VERBOSE) System.out.print(value+" ");
								}
								if(MODE_VERBOSE) System.out.print("\n");
								if(light.getTechniqueCommon().getPoint().getConstantAttenuation() != null){
									fileOut.writeFloat((float)light.getTechniqueCommon().getPoint().getConstantAttenuation().getValue());
									if(MODE_VERBOSE) System.out.println("                > KC : "+light.getTechniqueCommon().getPoint().getConstantAttenuation().getValue());
								}
								else{
									fileOut.writeFloat(DEFAULT_LIGHT_KC);
									if(MODE_VERBOSE) System.out.println("                > KC : "+DEFAULT_LIGHT_KC);
								}
								if(light.getTechniqueCommon().getPoint().getLinearAttenuation() != null){
									fileOut.writeFloat((float)light.getTechniqueCommon().getPoint().getLinearAttenuation().getValue());
									if(MODE_VERBOSE) System.out.println("                > KL : "+light.getTechniqueCommon().getPoint().getLinearAttenuation().getValue());
								}
								else{
									fileOut.writeFloat(DEFAULT_LIGHT_KL);
									if(MODE_VERBOSE) System.out.println("                > KL : "+DEFAULT_LIGHT_KL);
								}
								if(light.getTechniqueCommon().getPoint().getQuadraticAttenuation() != null){
									fileOut.writeFloat((float)light.getTechniqueCommon().getPoint().getQuadraticAttenuation().getValue());
									if(MODE_VERBOSE) System.out.println("                > KQ : "+light.getTechniqueCommon().getPoint().getQuadraticAttenuation().getValue());
								}
								else{
									fileOut.writeFloat(DEFAULT_LIGHT_KQ);
									if(MODE_VERBOSE) System.out.println("                > KQ : "+DEFAULT_LIGHT_KQ);
								}
							}
							else{
								System.err.println("            [ERROR] Missing color element");
								errorCount++;
								continue;
							}
						}
						else if(light.getTechniqueCommon().getSpot() != null){
							if(MODE_VERBOSE) System.out.println("                > TYPE : spot");
							if(light.getTechniqueCommon().getSpot().getColor() != null){
								fileOut.writeInt(API.LIGHT);
								fileOut.writeInt(ID);
								fileOut.writeInt(API.SPOT);
								if(MODE_VERBOSE) System.out.print("                > RGB : ");
								for(Double value : light.getTechniqueCommon().getSpot().getColor().getValue()){
									fileOut.writeFloat(value.floatValue());
									if(MODE_VERBOSE) System.out.print(value+" ");
								}
								if(MODE_VERBOSE) System.out.print("\n");
								if(light.getTechniqueCommon().getSpot().getConstantAttenuation() != null){
									fileOut.writeFloat((float)light.getTechniqueCommon().getSpot().getConstantAttenuation().getValue());
									if(MODE_VERBOSE) System.out.println("                > KC : "+light.getTechniqueCommon().getSpot().getConstantAttenuation().getValue());
								}
								else{
									fileOut.writeFloat(DEFAULT_LIGHT_KC);
									if(MODE_VERBOSE) System.out.println("                > KC : "+DEFAULT_LIGHT_KC);
								}
								if(light.getTechniqueCommon().getSpot().getLinearAttenuation() != null){
									fileOut.writeFloat((float)light.getTechniqueCommon().getSpot().getLinearAttenuation().getValue());
									if(MODE_VERBOSE) System.out.println("                > KL : "+light.getTechniqueCommon().getSpot().getLinearAttenuation().getValue());
								}
								else{
									fileOut.writeFloat(DEFAULT_LIGHT_KL);
									if(MODE_VERBOSE) System.out.println("                > KL : "+DEFAULT_LIGHT_KL);
								}
								if(light.getTechniqueCommon().getSpot().getQuadraticAttenuation() != null){
									fileOut.writeFloat((float)light.getTechniqueCommon().getSpot().getQuadraticAttenuation().getValue());
									if(MODE_VERBOSE) System.out.println("                > KQ : "+light.getTechniqueCommon().getSpot().getQuadraticAttenuation().getValue());
								}
								else{
									fileOut.writeFloat(DEFAULT_LIGHT_KQ);
									if(MODE_VERBOSE) System.out.println("                > KQ : "+DEFAULT_LIGHT_KQ);
								}
								if(light.getTechniqueCommon().getSpot().getFalloffAngle() != null){
									fileOut.writeFloat((float)light.getTechniqueCommon().getSpot().getFalloffAngle().getValue());
									if(MODE_VERBOSE) System.out.println("                > ANGLE OFF : "+light.getTechniqueCommon().getSpot().getFalloffAngle().getValue());
								}
								else{
									fileOut.writeFloat(DEFAULT_LIGHT_FALLOFF_ANGLE);
									if(MODE_VERBOSE) System.out.println("                > ANGLE OFF : "+DEFAULT_LIGHT_FALLOFF_ANGLE);
								}
								if(light.getTechniqueCommon().getSpot().getFalloffExponent() != null){
									fileOut.writeFloat((float)light.getTechniqueCommon().getSpot().getFalloffExponent().getValue());
									if(MODE_VERBOSE) System.out.println("                > ANGLE EXP : "+light.getTechniqueCommon().getSpot().getFalloffExponent().getValue());
								}
								else{
									fileOut.writeFloat(DEFAULT_LIGHT_FALLOFF_EXP);
									if(MODE_VERBOSE) System.out.println("                > ANGLE EXP : "+DEFAULT_LIGHT_FALLOFF_EXP);
								}
								
							}
							else{
								System.err.println("            [ERROR] Missing color element");
								errorCount++;
								continue;
							}
						}
						else{
							System.err.println("            [ERROR] Unsupported TYPE");
							errorCount++;
							continue;
						}
						lightCount++;
					}
					if(lightCount == 0){
						if(MODE_VERBOSE) System.out.println("            > No light found");
					}
				}
				/************************************************************************************************
				 * CAMERA	- Describes the optics in the scene	
				 ************************************************************************************************
				 
				  [CAMERA][$ID][$CAMERA_TYPE]
					  case PERSPECTIVE : [$XFOV][$YFOV][$ASPECT_RATIO][$ZNEAR][$ZFAR][$UP_X][$UP_Y][$UP_Z]
					  case ORTHOGRAPHIC : [$XMAG][$YMAG][$ASPECT_RATIO][$ZNEAR][$ZFAR][$UP_X][$UP_Y][$UP_Z]
				  ...
				  [CAMERA][$ID]
				  
				 ************************************************************************************************/
				else if(element instanceof LibraryCameras){
					if(MODE_VERBOSE) System.out.println("    > Camera Elements");
					apiWriter.write("\n\t//LibraryCameras\n");
					for(Camera camera : ((LibraryCameras)element).getCamera()){
						if(MODE_VERBOSE) System.out.println("            > Parsing "+camera.getId());
						int ID = camera.getId().hashCode();
						
						if(MODE_VERBOSE) System.out.println("                > ID : "+ID);
						apiWriter.write("\tpublic static final int CAMERA_"+IDtoConst(camera.getId())+" = "+ID+";\n");
						
						if(camera.getOptics().getTechniqueCommon().getPerspective() != null){
							if(MODE_VERBOSE) System.out.println("                > TYPE : perspective");
							fileOut.writeInt(API.CAMERA);
							fileOut.writeInt(ID);
							fileOut.writeInt(API.PERSPECTIVE);
							float xFov = API.UNSPECIFIED;
							float yFov = API.UNSPECIFIED;
							float ratio = 1f;
							float zNear = DEFAULT_PERSPECTIVE_ZNEAR;
							float zFar = DEFAULT_PERSPECTIVE_ZFAR;
							float up[] = new float[]{0f,1f,0f};
							
							for(JAXBElement<TargetableFloat> config : camera.getOptics().getTechniqueCommon().getPerspective().getContent()){
								if(config.getName().getLocalPart().equals("xfov")) xFov = (float)config.getValue().getValue();
								else if(config.getName().getLocalPart().equals("yfov")) yFov = (float)config.getValue().getValue();
								else if(config.getName().getLocalPart().equals("aspect_ratio")) ratio = (float)config.getValue().getValue();
								else if(config.getName().getLocalPart().equals("znear")) zNear = (float)config.getValue().getValue();
								else if(config.getName().getLocalPart().equals("zfar")) zFar = (float)config.getValue().getValue();
							}
							if(xFov == API.UNSPECIFIED && yFov == API.UNSPECIFIED ){
								xFov = DEFAULT_PERSPECTIVE_XFOV;
								yFov = DEFAULT_PERSPECTIVE_YFOV;
							}
							else if(xFov == API.UNSPECIFIED) xFov = yFov*ratio;
							else yFov = xFov/ratio;
							
							if(collada.getAsset() != null && collada.getAsset().getUpAxis() != null){
								String strUp = collada.getAsset().getUpAxis().value();
								if(strUp.equals("X_UP")){
									up[0] = 1f;
									up[1] = 0f;
									up[2] = 0f;
								}
								else if(strUp.equals("Y_UP")){
									up[0] = 0f;
									up[1] = 1f;
									up[2] = 0f;
								}
								else if(strUp.equals("Z_UP")){
									up[0] = 0f;
									up[1] = 0f;
									up[2] = 1f;
								}
							}
							
							if(MODE_VERBOSE) System.out.println("                    > xFov : "+xFov);
							if(MODE_VERBOSE) System.out.println("                    > yFov : "+yFov);
							if(MODE_VERBOSE) System.out.println("                    > ratio : "+ratio);
							if(MODE_VERBOSE) System.out.println("                    > zNear : "+zNear);
							if(MODE_VERBOSE) System.out.println("                    > zFar : "+zFar);
							if(MODE_VERBOSE) System.out.println("                    > up : "+up[0]+" "+up[1]+" "+up[2]);
							
							fileOut.writeFloat(xFov);
							fileOut.writeFloat(yFov);
							fileOut.writeFloat(ratio);
							fileOut.writeFloat(zNear);
							fileOut.writeFloat(zFar);
							fileOut.writeFloat(up[0]);
							fileOut.writeFloat(up[1]);
							fileOut.writeFloat(up[2]);
						}
						else if(camera.getOptics().getTechniqueCommon().getOrthographic() != null){
							if(MODE_VERBOSE) System.out.println("                > TYPE : orthographic");
							fileOut.writeInt(API.CAMERA);
							fileOut.writeInt(ID);
							fileOut.writeInt(API.ORTHOGRAPHIC);
							float xMag = API.UNSPECIFIED;
							float yMag = API.UNSPECIFIED;
							float ratio = 1f;
							float zNear = DEFAULT_ORTHO_ZNEAR;
							float zFar = DEFAULT_ORTHO_ZFAR;
							float up[] = new float[]{0f,1f,0f};
							
							for(JAXBElement<TargetableFloat> config : camera.getOptics().getTechniqueCommon().getOrthographic().getContent()){
								if(config.getName().getLocalPart().equals("xmag")) xMag = (float)config.getValue().getValue();
								else if(config.getName().getLocalPart().equals("ymag")) yMag = (float)config.getValue().getValue();
								else if(config.getName().getLocalPart().equals("aspect_ratio")) ratio = (float)config.getValue().getValue();
								else if(config.getName().getLocalPart().equals("znear")) zNear = (float)config.getValue().getValue();
								else if(config.getName().getLocalPart().equals("zfar")) zFar = (float)config.getValue().getValue();
							}
							if(xMag == API.UNSPECIFIED && yMag == API.UNSPECIFIED ){
								xMag = DEFAULT_ORTHO_XMAG;
								yMag = DEFAULT_ORTHO_YMAG;
							}
							else if(xMag == API.UNSPECIFIED) xMag = yMag*ratio;
							else yMag = xMag/ratio;
							
							if(collada.getAsset() != null && collada.getAsset().getUpAxis() != null){
								String strUp = collada.getAsset().getUpAxis().value();
								if(strUp.equals("X_UP")){
									up[0] = 1f;
									up[1] = 0f;
									up[2] = 0f;
								}
								else if(strUp.equals("Y_UP")){
									up[0] = 0f;
									up[1] = 1f;
									up[2] = 0f;
								}
								else if(strUp.equals("Z_UP")){
									up[0] = 0f;
									up[1] = 0f;
									up[2] = 1f;
								}
							}
							
							if(MODE_VERBOSE) System.out.println("                    > xMag : "+xMag);
							if(MODE_VERBOSE) System.out.println("                    > yMag : "+yMag);
							if(MODE_VERBOSE) System.out.println("                    > ratio : "+ratio);
							if(MODE_VERBOSE) System.out.println("                    > zNear : "+zNear);
							if(MODE_VERBOSE) System.out.println("                    > zFar : "+zFar);
							if(MODE_VERBOSE) System.out.println("                    > up : "+up[0]+" "+up[1]+" "+up[2]);
							
							fileOut.writeFloat(xMag);
							fileOut.writeFloat(yMag);
							fileOut.writeFloat(ratio);
							fileOut.writeFloat(zNear);
							fileOut.writeFloat(zFar);
							fileOut.writeFloat(up[0]);
							fileOut.writeFloat(up[1]);
							fileOut.writeFloat(up[2]);
						}
						else{
							System.err.println("            [ERROR] Unsupported TYPE");
							errorCount++;
							continue;
						}
						cameraCount++;
					}
					if(cameraCount == 0){
						if(MODE_VERBOSE) System.out.println("            > No camera found");
					}
				}
				/************************************************************************************************
				 * IMAGERY	- Describes the images in the scene	
				 ************************************************************************************************
				 
				  [IMAGE][$ID][$FORMAT][$INTERNAL_FORMAT][$TYPE][$WIDTH][$HEIGHT][$COMPRESS][$MAG][$MIN][$PATH]
				  ...
				  [IMAGE][$ID][$FORMAT][$INTERNAL_FORMAT][$TYPE][$WIDTH][$HEIGHT][$COMPRESS][$MAG][$MIN][$PATH]
				  
				 ************************************************************************************************/
				else if(element instanceof LibraryImages){
					if(MODE_VERBOSE) System.out.println("    > Image Elements");
					apiWriter.write("\n\t//LibraryImages\n");
					for(Image image : ((LibraryImages)element).getImage()){
						if(MODE_VERBOSE) System.out.println("            > Parsing "+image.getId());
						int ID = image.getId().hashCode();
						
						if(MODE_VERBOSE) System.out.println("                > ID : "+ID);
						apiWriter.write("\tpublic static final int IMAGE_"+IDtoConst(image.getId())+" = "+ID+";\n");
						
						if(image.getInitFrom() != null){
							int iFormat = API.UNSPECIFIED;
							int iiFormat = API.UNSPECIFIED;
							int iType = API.UNSPECIFIED;
							int iWidth = API.UNSPECIFIED;
							int iHeight = API.UNSPECIFIED;
							boolean iCompress = false;
							int iMag = API.UNSPECIFIED;
							int iMin = API.UNSPECIFIED;
							if((image.getFormat() != null && image.getFormat().equalsIgnoreCase("jpg")) 
									|| image.getInitFrom().endsWith(".jpg") || image.getInitFrom().endsWith(".jpeg")){
								iFormat = API.JPG;
							}
							else if((image.getFormat() != null && image.getFormat().equalsIgnoreCase("png"))
									|| image.getInitFrom().endsWith(".png")){
								iFormat = API.PNG;
							}
							else if((image.getFormat() != null && image.getFormat().equalsIgnoreCase("gif"))
									|| image.getInitFrom().endsWith(".gif")){
								iFormat = API.GIF;
							}
							else if((image.getFormat() != null && image.getFormat().equalsIgnoreCase("bmp"))
									|| image.getInitFrom().endsWith(".bmp")){
								iFormat = API.BMP;
							}
							if(image.getWidth() != null) iWidth = image.getWidth().intValue();
							if(image.getHeight() != null) iHeight = image.getHeight().intValue();
							
							
							File imageFile = new File(inFile.getParent()+"/"+image.getInitFrom()); 
							if(imageFile.exists()){
								BufferedImage bufferedImage = ImageIO.read(imageFile);
								if(iWidth == API.UNSPECIFIED) iWidth = bufferedImage.getWidth();
								if(iHeight == API.UNSPECIFIED) iHeight = bufferedImage.getHeight();
								switch(bufferedImage.getType()){
									case BufferedImage.TYPE_INT_RGB :
									case BufferedImage.TYPE_INT_BGR :
										iiFormat = API.RGB;
										iType = API.UNSIGNED_BYTE;
										break;
									case BufferedImage.TYPE_3BYTE_BGR :
										iiFormat = API.RGB;
										iType = API.UNSIGNED_BYTE;
										break;
									case BufferedImage.TYPE_INT_ARGB_PRE :
									case BufferedImage.TYPE_INT_ARGB :
									case BufferedImage.TYPE_4BYTE_ABGR :
									case BufferedImage.TYPE_4BYTE_ABGR_PRE :
										iiFormat = API.RGBA;
										iType = API.UNSIGNED_BYTE;
										break;
									case BufferedImage.TYPE_USHORT_565_RGB :
										iiFormat = API.RGB;
										iType = API.UNSIGNED_SHORT_5_6_5;
										break;
									case BufferedImage.TYPE_USHORT_555_RGB :
										iiFormat = API.RGB;
										iType = API.UNSIGNED_SHORT_5_5_5_1;
										break;
									default:
										System.err.println("            [ERROR] Unsupported image format : "+bufferedImage.toString());
										errorCount++;
										continue;
								}
							}
							else{
								System.err.println("            [ERROR] Image file not found : "+image.getInitFrom());
								errorCount++;
								continue;
							}
							
							
							for(Extra extra : image.getExtra()){
								for(Technique technique : extra.getTechnique()){
									if(technique.getProfile().equals("lgl")){
										for(Object object : technique.getAny()){
											if(object instanceof Param && ((Param)object).getName().equals("format")){
												String siFormat = ((Param)object).getValue().trim();
												if(siFormat.equals("RGB")) iiFormat = API.RGB;
												else if(siFormat.equals("RGBA")) iiFormat = API.RGBA;
											}
											else if(object instanceof Param && ((Param)object).getName().equals("type")){
												String sType = ((Param)object).getValue().trim();
												if(sType.equals("TYPE_UNSIGNED_BYTE")) iType = API.UNSIGNED_BYTE;
												else if(sType.equals("TYPE_UNSIGNED_SHORT_5_6_5")) iType = API.UNSIGNED_SHORT_5_6_5;
											}
											else if(object instanceof Param && ((Param)object).getName().equals("compress")){
												iCompress = Boolean.valueOf(((Param)object).getValue().trim());
											}
											else if(object instanceof Param && ((Param)object).getName().equals("min")){
												String sMin = ((Param)object).getValue().trim();
												if(sMin.equals("NEAREST")) iMin = API.NEAREST;
												else if(sMin.equals("LINEAR")) iMin = API.LINEAR;
												else if(sMin.equals("NEAREST_MIPMAP_NEAREST")) iMin = API.NEAREST_MIPMAP_NEAREST;
												else if(sMin.equals("NEAREST_MIPMAP_LINEAR")) iMin = API.NEAREST_MIPMAP_LINEAR;
												else if(sMin.equals("LINEAR_MIPMAP_LINEAR")) iMin = API.LINEAR_MIPMAP_LINEAR;
											}
											else if(object instanceof Param && ((Param)object).getName().equals("mag")){
												String sMag = ((Param)object).getValue().trim();
												if(sMag.equals("NEAREST")) iMag = API.NEAREST;
												else if(sMag.equals("LINEAR")) iMag = API.LINEAR;
											}
											
										}
									}
								}
							}
							
							
							if(iiFormat == API.UNSPECIFIED || iType == API.UNSPECIFIED){
								System.err.println("            [ERROR] Failed to analyse image : "+image.getInitFrom());
								errorCount++;
								continue;
							}
							
							if(MODE_VERBOSE) System.out.println("                > format : "+iFormat);
							if(MODE_VERBOSE) System.out.println("                > internal format : "+iiFormat);
							if(MODE_VERBOSE) System.out.println("                > type : "+iType);
							if(MODE_VERBOSE) System.out.println("                > width : "+iWidth);
							if(MODE_VERBOSE) System.out.println("                > height : "+iHeight);
							if(MODE_VERBOSE) System.out.println("                > compress : "+iCompress);
							if(MODE_VERBOSE) System.out.println("                > mag : "+iMag);
							if(MODE_VERBOSE) System.out.println("                > min : "+iMin);
							if(MODE_VERBOSE) System.out.println("                > path : "+image.getInitFrom());
							
							fileOut.writeInt(API.IMAGERY);
							fileOut.writeInt(ID);
							fileOut.writeInt(iFormat);
							fileOut.writeInt(iiFormat);
							fileOut.writeInt(iType);
							fileOut.writeInt(iWidth);
							fileOut.writeInt(iHeight);
							fileOut.writeBoolean(iCompress);
							fileOut.writeInt(iMag);
							fileOut.writeInt(iMin);
							fileOut.writeChars(image.getInitFrom());
							fileOut.writeChar('\0');
						}
						else{
							System.err.println("            [ERROR] No image source found (no <init_from>)");
							errorCount++;
							continue;
						}
						imageCount++;
					}
					if(imageCount == 0){
						if(MODE_VERBOSE) System.out.println("            > No image found");
					}
				}
				/************************************************************************************************
				 * MATERIAL	- Describes the materials in the scene	
				 ************************************************************************************************
				 
				  [MATERIAL][$ID][$MATERIAL_TYPE]
					  [$EMISSION_VAR_TYPE][$EMISSION_VALUE]
					  [$REFLECTIVE_VAR_TYPE][$REFLECTIVE_VALUE]
					  [$REFLECTIVITY_VAR_TYPE][$REFLECTIVITY_VALUE]
					  [$TRANSPARENT_VAR_TYPE][$TRANSPARENT_VALUE]
					  [$TRANSPARENCY_VAR_TYPE][$TRANSPARENCY_VALUE]
					  [$REFRACTION_VAR_TYPE][$REFRACTION_VALUE]
					  [$AMBIENT_VAR_TYPE][$AMBIENT_VALUE]
					  [$DIFFUSE_VAR_TYPE][$DIFFUSE_VALUE]
					  [$SPECULAR_VAR_TYPE][$SPECULAR_VALUE]
					  [$SHININESS_VAR_TYPE][$SHININESS_VALUE]
				  ...
				  [MATERIAL][$ID][$MATERIAL_TYPE]
				  
				  $*_VAR_TYPE :
				  	case FLOAT : [$FLOAT_VALUE]
				  	case FLOAT3 : [$FLOAT_VALUE_0][$FLOAT_VALUE_1][$FLOAT_VALUE_2]
				  	case FLOAT4 : [$FLOAT_VALUE_0][$FLOAT_VALUE_1][$FLOAT_VALUE_2][$FLOAT_VALUE_3]
				  	case SAMPLER : [$IMAGERY_ID][$SEMANTIC]
				  	case UNSPECIFIED : -
				 ************************************************************************************************/
				else if(element instanceof LibraryMaterials){
					if(MODE_VERBOSE) System.out.println("    > Material Elements");
					apiWriter.write("\n\t//LibraryMaterials\n");
					for(Material material : ((LibraryMaterials)element).getMaterial()){
						if(MODE_VERBOSE) System.out.println("            > Parsing "+material.getId());
						
						int ID = material.getId().hashCode();
						int mType = 0;
						
						fileOut.writeInt(API.MATERIAL);
						fileOut.writeInt(ID);
						
						if(MODE_VERBOSE) System.out.println("                > ID : "+ID);
						apiWriter.write("\tpublic static final int MATERIAL_"+IDtoConst(material.getId())+" = "+ID+";\n");
						
						
						final InstanceEffect instanceEffect = material.getInstanceEffect();
						Effect sourceEffect = null;
						if(instanceEffect != null){
							int instanceEffectId = URIToID(instanceEffect.getUrl());
							if(libraryEffects != null){
								for(Effect effect : libraryEffects.getEffect()){
									if(instanceEffectId == effect.getId().hashCode()){
										sourceEffect = effect;
										break;
									}
									
								}
							}
						}
						if(sourceEffect != null){
							if(MODE_VERBOSE) System.out.println("                    > Effect : "+sourceEffect.getId().hashCode());
							ProfileCOMMON profileCommon = null;
							for(JAXBElement profile :  sourceEffect.getFxProfileAbstract()){
								if(profile.getName().getLocalPart().equals("profile_COMMON")){
									profileCommon = (ProfileCOMMON)profile.getValue(); 
								}
							}
							
							if(profileCommon != null){
								ProfileCOMMON.Technique technique = profileCommon.getTechnique();
								if(technique != null){
									//CONSTANT MATERIAL
									if(technique.getConstant() != null){
										mType = API.CONSTANT;
										fileOut.writeInt(mType);

										if(MODE_VERBOSE) System.out.println("                        > Type : CONSTANT");
										if(MODE_VERBOSE) System.out.print("                            > Emission : ");
										if(technique.getConstant().getEmission() != null){
											if(technique.getConstant().getEmission().getColor() != null){
												List<Double> colorValues = technique.getConstant().getEmission().getColor().getValue();
												fileOut.writeInt(API.FLOAT4);
												for(Double colorValue : colorValues){
													if(MODE_VERBOSE) System.out.print(colorValue+" ");
													fileOut.writeFloat(colorValue.floatValue());
												}
												if(colorValues.size() == 3){
													fileOut.writeFloat(1.0f);
												}
												if(MODE_VERBOSE) System.out.print("\n");
											}
											else if(technique.getConstant().getEmission().getTexture() != null){
												String paramName = technique.getConstant().getEmission().getTexture().getTexture();
												for(Object inObject : profileCommon.getImageOrNewparam()){
													if(inObject instanceof CommonNewparamType){
														if(((CommonNewparamType)inObject).getSid().equals(paramName)){
															fileOut.writeInt(API.SAMPLER);
															final FxSampler2DCommon sampler = ((CommonNewparamType)inObject).getSampler2D();
															String surfaceId = null;
															if(sampler != null){
																for(Object inObject2 : profileCommon.getImageOrNewparam()){
																	if(inObject2 instanceof CommonNewparamType){
																		if(((CommonNewparamType)inObject2).getSid().equals(sampler.getSource())){
																			if(((CommonNewparamType)inObject2).getSurface() != null){
																				surfaceId = ((Image)((CommonNewparamType)inObject2).getSurface().getInitFrom().get(0).getValue()).getId();
																			}
																		}
																	}
																}
																if(surfaceId != null){
																	if(MODE_VERBOSE) System.out.print(URIToID(surfaceId));
																	fileOut.writeInt(URIToID(surfaceId));
																	if(MODE_VERBOSE) System.out.println(" -> Semantic : "+URIToID(technique.getConstant().getEmission().getTexture().getTexcoord()));
																	fileOut.writeInt(URIToID(technique.getConstant().getEmission().getTexture().getTexcoord()));
																}
																else{
																	fileOut.writeInt(API.UNSPECIFIED);
																	errorCount++;
																	System.err.println("\n            [ERROR] Surface ID not found for sampler "+paramName);
																}
															}
															else{
																fileOut.writeInt(API.UNSPECIFIED);
																errorCount++;
																System.err.println("\n            [ERROR] Sampler not found "+paramName);
															}
														}
													}
												}
											}
											else if(technique.getConstant().getEmission().getParam() != null){
												errorCount++;
												System.err.println("\n            [ERROR] Params not supported in effect "+sourceEffect.getId());
											}
										}
										else{
											fileOut.writeInt(API.UNSPECIFIED);
											if(MODE_VERBOSE) System.out.println("not set");
										}
										if(MODE_VERBOSE) System.out.print("                            > Reflective : ");
										if(technique.getConstant().getReflective() != null){
											if(technique.getConstant().getReflective().getColor() != null){
												List<Double> colorValues = technique.getConstant().getReflective().getColor().getValue();
												fileOut.writeInt(API.FLOAT4);
												for(Double colorValue : colorValues){
													if(MODE_VERBOSE) System.out.print(colorValue+" ");
													fileOut.writeFloat(colorValue.floatValue());
												}
												if(colorValues.size() == 3){
													fileOut.writeFloat(1.0f);
												}
												if(MODE_VERBOSE) System.out.print("\n");
											}
											else if(technique.getConstant().getReflective().getTexture() != null){
												String paramName = technique.getConstant().getReflective().getTexture().getTexture();
												for(Object inObject : profileCommon.getImageOrNewparam()){
													if(inObject instanceof CommonNewparamType){
														if(((CommonNewparamType)inObject).getSid().equals(paramName)){
															fileOut.writeInt(API.SAMPLER);
															final FxSampler2DCommon sampler = ((CommonNewparamType)inObject).getSampler2D();
															String surfaceId = null;
															if(sampler != null){
																for(Object inObject2 : profileCommon.getImageOrNewparam()){
																	if(inObject2 instanceof CommonNewparamType){
																		if(((CommonNewparamType)inObject2).getSid().equals(sampler.getSource())){
																			if(((CommonNewparamType)inObject2).getSurface() != null){
																				surfaceId = ((Image)((CommonNewparamType)inObject2).getSurface().getInitFrom().get(0).getValue()).getId();
																			}
																		}
																	}
																}
																if(surfaceId != null){
																	if(MODE_VERBOSE) System.out.print(URIToID(surfaceId));
																	fileOut.writeInt(URIToID(surfaceId));
																	if(MODE_VERBOSE) System.out.println(" -> Semantic : "+URIToID(technique.getConstant().getReflective().getTexture().getTexcoord()));
																	fileOut.writeInt(URIToID(technique.getConstant().getReflective().getTexture().getTexcoord()));
																}
																else{
																	fileOut.writeInt(API.UNSPECIFIED);
																	errorCount++;
																	System.err.println("\n            [ERROR] Surface ID not found for sampler "+paramName);
																}
															}
															else{
																fileOut.writeInt(API.UNSPECIFIED);
																errorCount++;
																System.err.println("\n            [ERROR] Sampler not found "+paramName);
															}
														}
													}
												}
											}
											else if(technique.getConstant().getReflective().getParam() != null){
												errorCount++;
												System.err.println("\n            [ERROR] Params not supported in effect "+sourceEffect.getId());
											}
										}
										else{
											fileOut.writeInt(API.UNSPECIFIED);
											if(MODE_VERBOSE) System.out.println("not set");
										}
										if(MODE_VERBOSE) System.out.print("                            > Reflectivity : ");
										if(technique.getConstant().getReflectivity() != null){
											if(technique.getConstant().getReflectivity().getFloat() != null){
												fileOut.writeInt(API.FLOAT);
												fileOut.writeFloat((float)technique.getConstant().getReflectivity().getFloat().getValue());
												if(MODE_VERBOSE) System.out.println((float)technique.getConstant().getReflectivity().getFloat().getValue());
											}
											else if(technique.getConstant().getReflectivity().getParam() != null){
												errorCount++;
												System.err.println("\n            [ERROR] Params not supported in effect "+sourceEffect.getId());
											}
										}
										else{
											fileOut.writeInt(API.UNSPECIFIED);
											if(MODE_VERBOSE) System.out.println("not set");
										}
										if(MODE_VERBOSE) System.out.print("                            > Transparent : ");
										if(technique.getConstant().getTransparent() != null){
											if(technique.getConstant().getTransparent().getColor() != null){
												List<Double> colorValues = technique.getConstant().getTransparent().getColor().getValue();
												fileOut.writeInt(API.FLOAT4);
												for(Double colorValue : colorValues){
													if(MODE_VERBOSE) System.out.print(colorValue+" ");
													fileOut.writeFloat(colorValue.floatValue());
												}
												if(colorValues.size() == 3){
													fileOut.writeFloat(1.0f);
												}
												if(MODE_VERBOSE) System.out.print("\n");
											}
											else if(technique.getConstant().getTransparent().getTexture() != null){
												String paramName = technique.getConstant().getTransparent().getTexture().getTexture();
												for(Object inObject : profileCommon.getImageOrNewparam()){
													if(inObject instanceof CommonNewparamType){
														if(((CommonNewparamType)inObject).getSid().equals(paramName)){
															fileOut.writeInt(API.SAMPLER);
															final FxSampler2DCommon sampler = ((CommonNewparamType)inObject).getSampler2D();
															String surfaceId = null;
															if(sampler != null){
																for(Object inObject2 : profileCommon.getImageOrNewparam()){
																	if(inObject2 instanceof CommonNewparamType){
																		if(((CommonNewparamType)inObject2).getSid().equals(sampler.getSource())){
																			if(((CommonNewparamType)inObject2).getSurface() != null){
																				surfaceId = ((Image)((CommonNewparamType)inObject2).getSurface().getInitFrom().get(0).getValue()).getId();
																			}
																		}
																	}
																}
																if(surfaceId != null){
																	if(MODE_VERBOSE) System.out.print(URIToID(surfaceId));
																	fileOut.writeInt(URIToID(surfaceId));
																	if(MODE_VERBOSE) System.out.println(" -> Semantic : "+URIToID(technique.getConstant().getTransparent().getTexture().getTexcoord()));
																	fileOut.writeInt(URIToID(technique.getConstant().getTransparent().getTexture().getTexcoord()));
																}
																else{
																	fileOut.writeInt(API.UNSPECIFIED);
																	errorCount++;
																	System.err.println("\n            [ERROR] Surface ID not found for sampler "+paramName);
																}
															}
															else{
																fileOut.writeInt(API.UNSPECIFIED);
																errorCount++;
																System.err.println("\n            [ERROR] Sampler not found "+paramName);
															}
														}
													}
												}
											}
											else if(technique.getConstant().getTransparent().getParam() != null){
												errorCount++;
												System.err.println("\n            [ERROR] Params not supported in effect "+sourceEffect.getId());
											}
										}
										else{
											fileOut.writeInt(API.UNSPECIFIED);
											if(MODE_VERBOSE) System.out.println("not set");
										}
										if(MODE_VERBOSE) System.out.print("                            > Transparency : ");
										if(technique.getConstant().getTransparency() != null){
											if(technique.getConstant().getTransparency().getFloat() != null){
												fileOut.writeInt(API.FLOAT);
												fileOut.writeFloat((float)technique.getConstant().getTransparency().getFloat().getValue());
												if(MODE_VERBOSE) System.out.println((float)technique.getConstant().getTransparency().getFloat().getValue());
											}
											else if(technique.getConstant().getTransparency().getParam() != null){
												errorCount++;
												System.err.println("\n            [ERROR] Params not supported in effect "+sourceEffect.getId());
											}
										}
										else{
											fileOut.writeInt(API.UNSPECIFIED);
											if(MODE_VERBOSE) System.out.println("not set");
										}
										if(MODE_VERBOSE) System.out.print("                            > Refraction : ");
										if(technique.getConstant().getIndexOfRefraction() != null){
											if(technique.getConstant().getIndexOfRefraction().getFloat() != null){
												fileOut.writeInt(API.FLOAT);
												fileOut.writeFloat((float)technique.getConstant().getIndexOfRefraction().getFloat().getValue());
												if(MODE_VERBOSE) System.out.println((float)technique.getConstant().getIndexOfRefraction().getFloat().getValue());
											}
											else if(technique.getConstant().getIndexOfRefraction().getParam() != null){
												errorCount++;
												System.err.println("\n            [ERROR] Params not supported in effect "+sourceEffect.getId());
											}
										}
										else{
											fileOut.writeInt(API.UNSPECIFIED);
											if(MODE_VERBOSE) System.out.println("not set");
										}
										if(MODE_VERBOSE) System.out.print("                            > Ambient : ");
										fileOut.writeInt(API.UNSPECIFIED);
										if(MODE_VERBOSE) System.out.println("not set");
										if(MODE_VERBOSE) System.out.print("                            > Diffuse : ");
										fileOut.writeInt(API.UNSPECIFIED);
										if(MODE_VERBOSE) System.out.println("not set");
										if(MODE_VERBOSE) System.out.print("                            > Specular : ");
										fileOut.writeInt(API.UNSPECIFIED);
										if(MODE_VERBOSE) System.out.println("not set");
										if(MODE_VERBOSE) System.out.print("                            > Shininess : ");
										fileOut.writeInt(API.UNSPECIFIED);
										if(MODE_VERBOSE) System.out.println("not set");
									}
									//LAMBERT MATERIAL
									else if(technique.getLambert() != null){
										mType = API.DIFFUSE;		
										fileOut.writeInt(mType);
										
										if(MODE_VERBOSE) System.out.println("                        > Type : DIFFUSE");
										if(MODE_VERBOSE) System.out.print("                            > Emission : ");
										if(technique.getLambert().getEmission() != null){
											if(technique.getLambert().getEmission().getColor() != null){
												List<Double> colorValues = technique.getLambert().getEmission().getColor().getValue();
												fileOut.writeInt(API.FLOAT4);
												for(Double colorValue : colorValues){
													if(MODE_VERBOSE) System.out.print(colorValue+" ");
													fileOut.writeFloat(colorValue.floatValue());
												}
												if(colorValues.size() == 3){
													fileOut.writeFloat(1.0f);
												}
												if(MODE_VERBOSE) System.out.print("\n");
											}										
											else if(technique.getLambert().getEmission().getTexture() != null){
												String paramName = technique.getLambert().getEmission().getTexture().getTexture();
												for(Object inObject : profileCommon.getImageOrNewparam()){
													if(inObject instanceof CommonNewparamType){
														if(((CommonNewparamType)inObject).getSid().equals(paramName)){
															fileOut.writeInt(API.SAMPLER);
															final FxSampler2DCommon sampler = ((CommonNewparamType)inObject).getSampler2D();
															String surfaceId = null;
															if(sampler != null){
																for(Object inObject2 : profileCommon.getImageOrNewparam()){
																	if(inObject2 instanceof CommonNewparamType){
																		if(((CommonNewparamType)inObject2).getSid().equals(sampler.getSource())){
																			if(((CommonNewparamType)inObject2).getSurface() != null){
																				surfaceId = ((Image)((CommonNewparamType)inObject2).getSurface().getInitFrom().get(0).getValue()).getId();
																			}
																		}
																	}
																}
																if(surfaceId != null){
																	if(MODE_VERBOSE) System.out.print(URIToID(surfaceId));
																	fileOut.writeInt(URIToID(surfaceId));
																	if(MODE_VERBOSE) System.out.println(" -> Semantic : "+URIToID(technique.getLambert().getEmission().getTexture().getTexcoord()));
																	fileOut.writeInt(URIToID(technique.getLambert().getEmission().getTexture().getTexcoord()));
																}
																else{
																	fileOut.writeInt(API.UNSPECIFIED);
																	errorCount++;
																	System.err.println("\n            [ERROR] Surface ID not found for sampler "+paramName);
																}
															}
															else{
																fileOut.writeInt(API.UNSPECIFIED);
																errorCount++;
																System.err.println("\n            [ERROR] Sampler not found "+paramName);
															}
														}
													}
												}
											}
											else if(technique.getLambert().getEmission().getParam() != null){
												errorCount++;
												System.err.println("            [ERROR] Params not supported in effect "+sourceEffect.getId());
											}
										}
										else{
											fileOut.writeInt(API.UNSPECIFIED);
											if(MODE_VERBOSE) System.out.println("not set");
										}
										if(MODE_VERBOSE) System.out.print("                            > Reflective : ");
										if(technique.getLambert().getReflective() != null){
											if(technique.getLambert().getReflective().getColor() != null){
												List<Double> colorValues = technique.getLambert().getReflective().getColor().getValue();
												fileOut.writeInt(API.FLOAT4);
												for(Double colorValue : colorValues){
													if(MODE_VERBOSE) System.out.print(colorValue+" ");
													fileOut.writeFloat(colorValue.floatValue());
												}
												if(colorValues.size() == 3){
													fileOut.writeFloat(1.0f);
												}
												if(MODE_VERBOSE) System.out.print("\n");
											}
											else if(technique.getLambert().getReflective().getTexture() != null){
												String paramName = technique.getLambert().getReflective().getTexture().getTexture();
												for(Object inObject : profileCommon.getImageOrNewparam()){
													if(inObject instanceof CommonNewparamType){
														if(((CommonNewparamType)inObject).getSid().equals(paramName)){
															fileOut.writeInt(API.SAMPLER);
															final FxSampler2DCommon sampler = ((CommonNewparamType)inObject).getSampler2D();
															String surfaceId = null;
															if(sampler != null){
																for(Object inObject2 : profileCommon.getImageOrNewparam()){
																	if(inObject2 instanceof CommonNewparamType){
																		if(((CommonNewparamType)inObject2).getSid().equals(sampler.getSource())){
																			if(((CommonNewparamType)inObject2).getSurface() != null){
																				surfaceId = ((Image)((CommonNewparamType)inObject2).getSurface().getInitFrom().get(0).getValue()).getId();
																			}
																		}
																	}
																}
																if(surfaceId != null){
																	if(MODE_VERBOSE) System.out.print(URIToID(surfaceId));
																	fileOut.writeInt(URIToID(surfaceId));
																	if(MODE_VERBOSE) System.out.println(" -> Semantic : "+URIToID(technique.getLambert().getReflective().getTexture().getTexcoord()));
																	fileOut.writeInt(URIToID(technique.getLambert().getReflective().getTexture().getTexcoord()));
																}
																else{
																	fileOut.writeInt(API.UNSPECIFIED);
																	errorCount++;
																	System.err.println("\n            [ERROR] Surface ID not found for sampler "+paramName);
																}
															}
															else{
																fileOut.writeInt(API.UNSPECIFIED);
																errorCount++;
																System.err.println("\n            [ERROR] Sampler not found "+paramName);
															}
														}
													}
												}
											}
											else if(technique.getLambert().getReflective().getParam() != null){
												errorCount++;
												System.err.println("\n            [ERROR] Params not supported in effect "+sourceEffect.getId());
											}
										}
										else{
											fileOut.writeInt(API.UNSPECIFIED);
											if(MODE_VERBOSE) System.out.println("not set");
										}
										if(MODE_VERBOSE) System.out.print("                            > Reflectivity : ");
										if(technique.getLambert().getReflectivity() != null){
											if(technique.getLambert().getReflectivity().getFloat() != null){
												fileOut.writeInt(API.FLOAT);
												fileOut.writeFloat((float)technique.getLambert().getReflectivity().getFloat().getValue());
												if(MODE_VERBOSE) System.out.println((float)technique.getLambert().getReflectivity().getFloat().getValue());
											}
											else if(technique.getLambert().getReflectivity().getParam() != null){
												errorCount++;
												System.err.println("\n            [ERROR] Params not supported in effect "+sourceEffect.getId());
											}
										}
										else{
											fileOut.writeInt(API.UNSPECIFIED);
											if(MODE_VERBOSE) System.out.println("not set");
										}
										if(MODE_VERBOSE) System.out.print("                            > Transparent : ");
										if(technique.getLambert().getTransparent() != null){
											if(technique.getLambert().getTransparent().getColor() != null){
												List<Double> colorValues = technique.getLambert().getTransparent().getColor().getValue();
												fileOut.writeInt(API.FLOAT4);
												for(Double colorValue : colorValues){
													if(MODE_VERBOSE) System.out.print(colorValue+" ");
													fileOut.writeFloat(colorValue.floatValue());
												}
												if(colorValues.size() == 3){
													fileOut.writeFloat(1.0f);
												}
												if(MODE_VERBOSE) System.out.print("\n");
											}
											else if(technique.getLambert().getTransparent().getTexture() != null){
												String paramName = technique.getLambert().getTransparent().getTexture().getTexture();
												for(Object inObject : profileCommon.getImageOrNewparam()){
													if(inObject instanceof CommonNewparamType){
														if(((CommonNewparamType)inObject).getSid().equals(paramName)){
															fileOut.writeInt(API.SAMPLER);
															final FxSampler2DCommon sampler = ((CommonNewparamType)inObject).getSampler2D();
															String surfaceId = null;
															if(sampler != null){
																for(Object inObject2 : profileCommon.getImageOrNewparam()){
																	if(inObject2 instanceof CommonNewparamType){
																		if(((CommonNewparamType)inObject2).getSid().equals(sampler.getSource())){
																			if(((CommonNewparamType)inObject2).getSurface() != null){
																				surfaceId = ((Image)((CommonNewparamType)inObject2).getSurface().getInitFrom().get(0).getValue()).getId();
																			}
																		}
																	}
																}
																if(surfaceId != null){
																	if(MODE_VERBOSE) System.out.print(URIToID(surfaceId));
																	fileOut.writeInt(URIToID(surfaceId));
																	if(MODE_VERBOSE) System.out.println(" -> Semantic : "+URIToID(technique.getLambert().getTransparent().getTexture().getTexcoord()));
																	fileOut.writeInt(URIToID(technique.getLambert().getTransparent().getTexture().getTexcoord()));
																}
																else{
																	fileOut.writeInt(API.UNSPECIFIED);
																	errorCount++;
																	System.err.println("\n            [ERROR] Surface ID not found for sampler "+paramName);
																}
															}
															else{
																fileOut.writeInt(API.UNSPECIFIED);
																errorCount++;
																System.err.println("\n            [ERROR] Sampler not found "+paramName);
															}
														}
													}
												}
											}
											else if(technique.getLambert().getTransparent().getParam() != null){
												errorCount++;
												System.err.println("\n            [ERROR] Params not supported in effect "+sourceEffect.getId());
											}
										}
										else{
											fileOut.writeInt(API.UNSPECIFIED);
											if(MODE_VERBOSE) System.out.println("not set");
										}
										if(MODE_VERBOSE) System.out.print("                            > Transparency : ");
										if(technique.getLambert().getTransparency() != null){
											if(technique.getLambert().getTransparency().getFloat() != null){
												fileOut.writeInt(API.FLOAT);
												fileOut.writeFloat((float)technique.getLambert().getTransparency().getFloat().getValue());
												if(MODE_VERBOSE) System.out.println((float)technique.getLambert().getTransparency().getFloat().getValue());
											}
											else if(technique.getLambert().getTransparency().getParam() != null){
												errorCount++;
												System.err.println("\n            [ERROR] Params not supported in effect "+sourceEffect.getId());
											}
										}
										else{
											fileOut.writeInt(API.UNSPECIFIED);
											if(MODE_VERBOSE) System.out.println("not set");
										}
										if(MODE_VERBOSE) System.out.print("                            > Refraction : ");
										if(technique.getLambert().getIndexOfRefraction() != null){
											if(technique.getLambert().getIndexOfRefraction().getFloat() != null){
												fileOut.writeInt(API.FLOAT);
												fileOut.writeFloat((float)technique.getLambert().getIndexOfRefraction().getFloat().getValue());
												if(MODE_VERBOSE) System.out.println((float)technique.getLambert().getIndexOfRefraction().getFloat().getValue());
											}
											else if(technique.getLambert().getIndexOfRefraction().getParam() != null){
												errorCount++;
												System.err.println("\n            [ERROR] Params not supported in effect "+sourceEffect.getId());
											}
										}
										else{
											fileOut.writeInt(API.UNSPECIFIED);
											if(MODE_VERBOSE) System.out.println("not set");
										}
										if(MODE_VERBOSE) System.out.print("                            > Ambient : ");
										if(technique.getLambert().getAmbient() != null){
											if(technique.getLambert().getAmbient().getColor() != null){
												List<Double> colorValues = technique.getLambert().getAmbient().getColor().getValue();
												fileOut.writeInt(API.FLOAT4);
												for(Double colorValue : colorValues){
													if(MODE_VERBOSE) System.out.print(colorValue+" ");
													fileOut.writeFloat(colorValue.floatValue());
												}
												if(colorValues.size() == 3){
													fileOut.writeFloat(1.0f);
												}
												if(MODE_VERBOSE) System.out.print("\n");
											}
											else if(technique.getLambert().getAmbient().getTexture() != null){
												String paramName = technique.getLambert().getAmbient().getTexture().getTexture();
												for(Object inObject : profileCommon.getImageOrNewparam()){
													if(inObject instanceof CommonNewparamType){
														if(((CommonNewparamType)inObject).getSid().equals(paramName)){
															fileOut.writeInt(API.SAMPLER);
															final FxSampler2DCommon sampler = ((CommonNewparamType)inObject).getSampler2D();
															String surfaceId = null;
															if(sampler != null){
																for(Object inObject2 : profileCommon.getImageOrNewparam()){
																	if(inObject2 instanceof CommonNewparamType){
																		if(((CommonNewparamType)inObject2).getSid().equals(sampler.getSource())){
																			if(((CommonNewparamType)inObject2).getSurface() != null){
																				surfaceId = ((Image)((CommonNewparamType)inObject2).getSurface().getInitFrom().get(0).getValue()).getId();
																			}
																		}
																	}
																}
																if(surfaceId != null){
																	if(MODE_VERBOSE) System.out.print(URIToID(surfaceId));
																	fileOut.writeInt(URIToID(surfaceId));
																	if(MODE_VERBOSE) System.out.println(" -> Semantic : "+URIToID(technique.getLambert().getAmbient().getTexture().getTexcoord()));
																	fileOut.writeInt(URIToID(technique.getLambert().getAmbient().getTexture().getTexcoord()));
																}
																else{
																	fileOut.writeInt(API.UNSPECIFIED);
																	errorCount++;
																	System.err.println("\n            [ERROR] Surface ID not found for sampler "+paramName);
																}
															}
															else{
																fileOut.writeInt(API.UNSPECIFIED);
																errorCount++;
																System.err.println("\n            [ERROR] Sampler not found "+paramName);
															}
														}
													}
												}
											}
											else if(technique.getLambert().getAmbient().getParam() != null){
												errorCount++;
												System.err.println("\n            [ERROR] Params not supported in effect "+sourceEffect.getId());
											}
										}
										else{
											fileOut.writeInt(API.UNSPECIFIED);
											if(MODE_VERBOSE) System.out.println("not set");
										}
										if(MODE_VERBOSE) System.out.print("                            > Diffuse : ");
										if(technique.getLambert().getDiffuse() != null){
											if(technique.getLambert().getDiffuse().getColor() != null){
												List<Double> colorValues = technique.getLambert().getDiffuse().getColor().getValue();
												fileOut.writeInt(API.FLOAT4);
												for(Double colorValue : colorValues){
													if(MODE_VERBOSE) System.out.print(colorValue+" ");
													fileOut.writeFloat(colorValue.floatValue());
												}
												if(colorValues.size() == 3){
													fileOut.writeFloat(1.0f);
												}
												if(MODE_VERBOSE) System.out.print("\n");
											}
											else if(technique.getLambert().getDiffuse().getTexture() != null){
												String paramName = technique.getLambert().getDiffuse().getTexture().getTexture();
												for(Object inObject : profileCommon.getImageOrNewparam()){
													if(inObject instanceof CommonNewparamType){
														if(((CommonNewparamType)inObject).getSid().equals(paramName)){
															fileOut.writeInt(API.SAMPLER);
															final FxSampler2DCommon sampler = ((CommonNewparamType)inObject).getSampler2D();
															String surfaceId = null;
															if(sampler != null){
																for(Object inObject2 : profileCommon.getImageOrNewparam()){
																	if(inObject2 instanceof CommonNewparamType){
																		if(((CommonNewparamType)inObject2).getSid().equals(sampler.getSource())){
																			if(((CommonNewparamType)inObject2).getSurface() != null){
																				surfaceId = ((Image)((CommonNewparamType)inObject2).getSurface().getInitFrom().get(0).getValue()).getId();
																			}
																		}
																	}
																}
																if(surfaceId != null){
																	if(MODE_VERBOSE) System.out.print(URIToID(surfaceId));
																	fileOut.writeInt(URIToID(surfaceId));
																	if(MODE_VERBOSE) System.out.println(" -> Semantic : "+URIToID(technique.getLambert().getDiffuse().getTexture().getTexcoord()));
																	fileOut.writeInt(URIToID(technique.getLambert().getDiffuse().getTexture().getTexcoord()));
																}
																else{
																	fileOut.writeInt(API.UNSPECIFIED);
																	errorCount++;
																	System.err.println("\n            [ERROR] Surface ID not found for sampler "+paramName);
																}
															}
															else{
																fileOut.writeInt(API.UNSPECIFIED);
																errorCount++;
																System.err.println("\n            [ERROR] Sampler not found "+paramName);
															}
														}
													}
												}
											}
											else if(technique.getLambert().getDiffuse().getParam() != null){
												errorCount++;
												System.err.println("\n            [ERROR] Params not supported in effect "+sourceEffect.getId());
											}
										}
										else{
											fileOut.writeInt(API.UNSPECIFIED);
											if(MODE_VERBOSE) System.out.println("not set");
										}
										if(MODE_VERBOSE) System.out.print("                            > Specular : ");
										fileOut.writeInt(API.UNSPECIFIED);
										if(MODE_VERBOSE) System.out.println("not set");
										if(MODE_VERBOSE) System.out.print("                            > Shininess : ");
										fileOut.writeInt(API.UNSPECIFIED);
										if(MODE_VERBOSE) System.out.println("not set");
									}
									//PHONG MATERIAL
									else if(technique.getPhong() != null){
										mType = API.SPECULAR;
										fileOut.writeInt(mType);
										
										if(MODE_VERBOSE) System.out.println("                        > Type : SPECULAR");
										if(MODE_VERBOSE) System.out.print("                            > Emission : ");
										if(technique.getPhong().getEmission() != null){
											if(technique.getPhong().getEmission().getColor() != null){
												List<Double> colorValues = technique.getPhong().getEmission().getColor().getValue();
												fileOut.writeInt(API.FLOAT4);
												for(Double colorValue : colorValues){
													if(MODE_VERBOSE) System.out.print(colorValue+" ");
													fileOut.writeFloat(colorValue.floatValue());
												}
												if(colorValues.size() == 3){
													fileOut.writeFloat(1.0f);
												}
												if(MODE_VERBOSE) System.out.print("\n");
											}
											else if(technique.getPhong().getEmission().getTexture() != null){
												String paramName = technique.getPhong().getEmission().getTexture().getTexture();
												for(Object inObject : profileCommon.getImageOrNewparam()){
													if(inObject instanceof CommonNewparamType){
														if(((CommonNewparamType)inObject).getSid().equals(paramName)){
															fileOut.writeInt(API.SAMPLER);
															final FxSampler2DCommon sampler = ((CommonNewparamType)inObject).getSampler2D();
															String surfaceId = null;
															if(sampler != null){
																for(Object inObject2 : profileCommon.getImageOrNewparam()){
																	if(inObject2 instanceof CommonNewparamType){
																		if(((CommonNewparamType)inObject2).getSid().equals(sampler.getSource())){
																			if(((CommonNewparamType)inObject2).getSurface() != null){
																				surfaceId = ((Image)((CommonNewparamType)inObject2).getSurface().getInitFrom().get(0).getValue()).getId();
																			}
																		}
																	}
																}
																if(surfaceId != null){
																	if(MODE_VERBOSE) System.out.print(URIToID(surfaceId));
																	fileOut.writeInt(URIToID(surfaceId));
																	if(MODE_VERBOSE) System.out.println(" -> Semantic : "+URIToID(technique.getPhong().getEmission().getTexture().getTexcoord()));
																	fileOut.writeInt(URIToID(technique.getPhong().getEmission().getTexture().getTexcoord()));
																}
																else{
																	fileOut.writeInt(API.UNSPECIFIED);
																	errorCount++;
																	System.err.println("\n            [ERROR] Surface ID not found for sampler "+paramName);
																}
															}
															else{
																fileOut.writeInt(API.UNSPECIFIED);
																errorCount++;
																System.err.println("\n            [ERROR] Sampler not found "+paramName);
															}
														}
													}
												}
											}
											else if(technique.getPhong().getEmission().getParam() != null){
												errorCount++;
												System.err.println("\n            [ERROR] Params not supported in effect "+sourceEffect.getId());
											}
										}
										else{
											fileOut.writeInt(API.UNSPECIFIED);
											if(MODE_VERBOSE) System.out.println("not set");
										}
										if(MODE_VERBOSE) System.out.print("                            > Reflective : ");
										if(technique.getPhong().getReflective() != null){
											if(technique.getPhong().getReflective().getColor() != null){
												List<Double> colorValues = technique.getPhong().getReflective().getColor().getValue();
												fileOut.writeInt(API.FLOAT4);
												for(Double colorValue : colorValues){
													if(MODE_VERBOSE) System.out.print(colorValue+" ");
													fileOut.writeFloat(colorValue.floatValue());
												}
												if(colorValues.size() == 3){
													fileOut.writeFloat(1.0f);
												}
												if(MODE_VERBOSE) System.out.print("\n");
											}
											else if(technique.getPhong().getReflective().getTexture() != null){
												String paramName = technique.getPhong().getReflective().getTexture().getTexture();
												for(Object inObject : profileCommon.getImageOrNewparam()){
													if(inObject instanceof CommonNewparamType){
														if(((CommonNewparamType)inObject).getSid().equals(paramName)){
															fileOut.writeInt(API.SAMPLER);
															final FxSampler2DCommon sampler = ((CommonNewparamType)inObject).getSampler2D();
															String surfaceId = null;
															if(sampler != null){
																for(Object inObject2 : profileCommon.getImageOrNewparam()){
																	if(inObject2 instanceof CommonNewparamType){
																		if(((CommonNewparamType)inObject2).getSid().equals(sampler.getSource())){
																			if(((CommonNewparamType)inObject2).getSurface() != null){
																				surfaceId = ((Image)((CommonNewparamType)inObject2).getSurface().getInitFrom().get(0).getValue()).getId();
																			}
																		}
																	}
																}
																if(surfaceId != null){
																	if(MODE_VERBOSE) System.out.print(URIToID(surfaceId));
																	fileOut.writeInt(URIToID(surfaceId));
																	if(MODE_VERBOSE) System.out.println(" -> Semantic : "+URIToID(technique.getPhong().getReflective().getTexture().getTexcoord()));
																	fileOut.writeInt(URIToID(technique.getPhong().getReflective().getTexture().getTexcoord()));
																}
																else{
																	fileOut.writeInt(API.UNSPECIFIED);
																	errorCount++;
																	System.err.println("\n            [ERROR] Surface ID not found for sampler "+paramName);
																}
															}
															else{
																fileOut.writeInt(API.UNSPECIFIED);
																errorCount++;
																System.err.println("\n            [ERROR] Sampler not found "+paramName);
															}
														}
													}
												}
											}
											else if(technique.getPhong().getReflective().getParam() != null){
												errorCount++;
												System.err.println("\n            [ERROR] Params not supported in effect "+sourceEffect.getId());
											}
										}
										else{
											fileOut.writeInt(API.UNSPECIFIED);
											if(MODE_VERBOSE) System.out.println("not set");
										}
										if(MODE_VERBOSE) System.out.print("                            > Reflectivity : ");
										if(technique.getPhong().getReflectivity() != null){
											if(technique.getPhong().getReflectivity().getFloat() != null){
												fileOut.writeInt(API.FLOAT);
												fileOut.writeFloat((float)technique.getPhong().getReflectivity().getFloat().getValue());
												if(MODE_VERBOSE) System.out.println((float)technique.getPhong().getReflectivity().getFloat().getValue());
											}
											else if(technique.getPhong().getReflectivity().getParam() != null){
												errorCount++;
												System.err.println("\n            [ERROR] Params not supported in effect "+sourceEffect.getId());
											}
										}
										else{
											fileOut.writeInt(API.UNSPECIFIED);
											if(MODE_VERBOSE) System.out.println("not set");
										}
										if(MODE_VERBOSE) System.out.print("                            > Transparent : ");
										if(technique.getPhong().getTransparent() != null){
											if(technique.getPhong().getTransparent().getColor() != null){
												List<Double> colorValues = technique.getPhong().getTransparent().getColor().getValue();
												fileOut.writeInt(API.FLOAT4);
												for(Double colorValue : colorValues){
													if(MODE_VERBOSE) System.out.print(colorValue+" ");
													fileOut.writeFloat(colorValue.floatValue());
												}
												if(colorValues.size() == 3){
													fileOut.writeFloat(1.0f);
												}
												if(MODE_VERBOSE) System.out.print("\n");
											}
											else if(technique.getPhong().getTransparent().getTexture() != null){
												String paramName = technique.getPhong().getTransparent().getTexture().getTexture();
												for(Object inObject : profileCommon.getImageOrNewparam()){
													if(inObject instanceof CommonNewparamType){
														if(((CommonNewparamType)inObject).getSid().equals(paramName)){
															fileOut.writeInt(API.SAMPLER);
															final FxSampler2DCommon sampler = ((CommonNewparamType)inObject).getSampler2D();
															String surfaceId = null;
															if(sampler != null){
																for(Object inObject2 : profileCommon.getImageOrNewparam()){
																	if(inObject2 instanceof CommonNewparamType){
																		if(((CommonNewparamType)inObject2).getSid().equals(sampler.getSource())){
																			if(((CommonNewparamType)inObject2).getSurface() != null){
																				surfaceId = ((Image)((CommonNewparamType)inObject2).getSurface().getInitFrom().get(0).getValue()).getId();
																			}
																		}
																	}
																}
																if(surfaceId != null){
																	if(MODE_VERBOSE) System.out.print(URIToID(surfaceId));
																	fileOut.writeInt(URIToID(surfaceId));
																	if(MODE_VERBOSE) System.out.println(" -> Semantic : "+URIToID(technique.getPhong().getTransparent().getTexture().getTexcoord()));
																	fileOut.writeInt(URIToID(technique.getPhong().getTransparent().getTexture().getTexcoord()));
																}
																else{
																	fileOut.writeInt(API.UNSPECIFIED);
																	errorCount++;
																	System.err.println("\n            [ERROR] Surface ID not found for sampler "+paramName);
																}
															}
															else{
																fileOut.writeInt(API.UNSPECIFIED);
																errorCount++;
																System.err.println("\n            [ERROR] Sampler not found "+paramName);
															}
														}
													}
												}
											}
											else if(technique.getPhong().getTransparent().getParam() != null){
												errorCount++;
												System.err.println("\n            [ERROR] Params not supported in effect "+sourceEffect.getId());
											}
										}
										else{
											fileOut.writeInt(API.UNSPECIFIED);
											if(MODE_VERBOSE) System.out.println("not set");
										}
										if(MODE_VERBOSE) System.out.print("                            > Transparency : ");
										if(technique.getPhong().getTransparency() != null){
											if(technique.getPhong().getTransparency().getFloat() != null){
												fileOut.writeInt(API.FLOAT);
												fileOut.writeFloat((float)technique.getPhong().getTransparency().getFloat().getValue());
												if(MODE_VERBOSE) System.out.println((float)technique.getPhong().getTransparency().getFloat().getValue());
											}
											else if(technique.getPhong().getTransparency().getParam() != null){
												errorCount++;
												System.err.println("\n            [ERROR] Params not supported in effect "+sourceEffect.getId());
											}
										}
										else{
											fileOut.writeInt(API.UNSPECIFIED);
											if(MODE_VERBOSE) System.out.println("not set");
										}
										if(MODE_VERBOSE) System.out.print("                            > Refraction : ");
										if(technique.getPhong().getIndexOfRefraction() != null){
											if(technique.getPhong().getIndexOfRefraction().getFloat() != null){
												fileOut.writeInt(API.FLOAT);
												fileOut.writeFloat((float)technique.getPhong().getIndexOfRefraction().getFloat().getValue());
												if(MODE_VERBOSE) System.out.println((float)technique.getPhong().getIndexOfRefraction().getFloat().getValue());
											}
											else if(technique.getPhong().getIndexOfRefraction().getParam() != null){
												errorCount++;
												System.err.println("\n            [ERROR] Params not supported in effect "+sourceEffect.getId());
											}
										}
										else{
											fileOut.writeInt(API.UNSPECIFIED);
											if(MODE_VERBOSE) System.out.println("not set");
										}
										if(MODE_VERBOSE) System.out.print("                            > Ambient : ");
										if(technique.getPhong().getAmbient() != null){
											if(technique.getPhong().getAmbient().getColor() != null){
												List<Double> colorValues = technique.getPhong().getAmbient().getColor().getValue();
												fileOut.writeInt(API.FLOAT4);
												for(Double colorValue : colorValues){
													if(MODE_VERBOSE) System.out.print(colorValue+" ");
													fileOut.writeFloat(colorValue.floatValue());
												}
												if(colorValues.size() == 3){
													fileOut.writeFloat(1.0f);
												}
												if(MODE_VERBOSE) System.out.print("\n");
											}
											else if(technique.getPhong().getAmbient().getTexture() != null){
												String paramName = technique.getPhong().getAmbient().getTexture().getTexture();
												for(Object inObject : profileCommon.getImageOrNewparam()){
													if(inObject instanceof CommonNewparamType){
														if(((CommonNewparamType)inObject).getSid().equals(paramName)){
															fileOut.writeInt(API.SAMPLER);
															final FxSampler2DCommon sampler = ((CommonNewparamType)inObject).getSampler2D();
															String surfaceId = null;
															if(sampler != null){
																for(Object inObject2 : profileCommon.getImageOrNewparam()){
																	if(inObject2 instanceof CommonNewparamType){
																		if(((CommonNewparamType)inObject2).getSid().equals(sampler.getSource())){
																			if(((CommonNewparamType)inObject2).getSurface() != null){
																				surfaceId = ((Image)((CommonNewparamType)inObject2).getSurface().getInitFrom().get(0).getValue()).getId();
																			}
																		}
																	}
																}
																if(surfaceId != null){
																	if(MODE_VERBOSE) System.out.print(URIToID(surfaceId));
																	fileOut.writeInt(URIToID(surfaceId));
																	if(MODE_VERBOSE) System.out.println(" -> Semantic : "+URIToID(technique.getPhong().getAmbient().getTexture().getTexcoord()));
																	fileOut.writeInt(URIToID(technique.getPhong().getAmbient().getTexture().getTexcoord()));
																}
																else{
																	fileOut.writeInt(API.UNSPECIFIED);
																	errorCount++;
																	System.err.println("\n            [ERROR] Surface ID not found for sampler "+paramName);
																}
															}
															else{
																fileOut.writeInt(API.UNSPECIFIED);
																errorCount++;
																System.err.println("\n            [ERROR] Sampler not found "+paramName);
															}
														}
													}
												}
											}
											else if(technique.getPhong().getAmbient().getParam() != null){
												errorCount++;
												System.err.println("\n            [ERROR] Params not supported in effect "+sourceEffect.getId());
											}
										}
										else{
											fileOut.writeInt(API.UNSPECIFIED);
											if(MODE_VERBOSE) System.out.println("not set");
										}
										if(MODE_VERBOSE) System.out.print("                            > Diffuse : ");
										if(technique.getPhong().getDiffuse() != null){
											if(technique.getPhong().getDiffuse().getColor() != null){
												List<Double> colorValues = technique.getPhong().getDiffuse().getColor().getValue();
												fileOut.writeInt(API.FLOAT4);
												for(Double colorValue : colorValues){
													if(MODE_VERBOSE) System.out.print(colorValue+" ");
													fileOut.writeFloat(colorValue.floatValue());
												}
												if(colorValues.size() == 3){
													fileOut.writeFloat(1.0f);
												}
												if(MODE_VERBOSE) System.out.print("\n");
											}
											else if(technique.getPhong().getDiffuse().getTexture() != null){
												String paramName = technique.getPhong().getDiffuse().getTexture().getTexture();
												for(Object inObject : profileCommon.getImageOrNewparam()){
													if(inObject instanceof CommonNewparamType){
														if(((CommonNewparamType)inObject).getSid().equals(paramName)){
															fileOut.writeInt(API.SAMPLER);
															final FxSampler2DCommon sampler = ((CommonNewparamType)inObject).getSampler2D();
															String surfaceId = null;
															if(sampler != null){
																for(Object inObject2 : profileCommon.getImageOrNewparam()){
																	if(inObject2 instanceof CommonNewparamType){
																		if(((CommonNewparamType)inObject2).getSid().equals(sampler.getSource())){
																			if(((CommonNewparamType)inObject2).getSurface() != null){
																				surfaceId = ((Image)((CommonNewparamType)inObject2).getSurface().getInitFrom().get(0).getValue()).getId();																			
																			}
																		}
																	}
																}
																if(surfaceId != null){
																	if(MODE_VERBOSE) System.out.print(URIToID(surfaceId));
																	fileOut.writeInt(URIToID(surfaceId));
																	if(MODE_VERBOSE) System.out.println(" -> Semantic : "+URIToID(technique.getPhong().getDiffuse().getTexture().getTexcoord()));
																	fileOut.writeInt(URIToID(technique.getPhong().getDiffuse().getTexture().getTexcoord()));
																}
																else{
																	fileOut.writeInt(API.UNSPECIFIED);
																	errorCount++;
																	System.err.println("\n            [ERROR] Surface ID not found for sampler "+paramName);
																}
															}
															else{
																fileOut.writeInt(API.UNSPECIFIED);
																errorCount++;
																System.err.println("\n            [ERROR] Sampler not found "+paramName);
															}
														}
													}
												}
											}
											else if(technique.getPhong().getDiffuse().getParam() != null){
												errorCount++;
												System.err.println("\n            [ERROR] Params not supported in effect "+sourceEffect.getId());
											}
										}
										else{
											fileOut.writeInt(API.UNSPECIFIED);
											if(MODE_VERBOSE) System.out.println("not set");
										}
										if(MODE_VERBOSE) System.out.print("                            > Specular : ");
										if(technique.getPhong().getSpecular() != null){
											if(technique.getPhong().getSpecular().getColor() != null){
												List<Double> colorValues = technique.getPhong().getSpecular().getColor().getValue();
												fileOut.writeInt(API.FLOAT4);
												for(Double colorValue : colorValues){
													if(MODE_VERBOSE) System.out.print(colorValue+" ");
													fileOut.writeFloat(colorValue.floatValue());
												}
												if(colorValues.size() == 3){
													fileOut.writeFloat(1.0f);
												}
												if(MODE_VERBOSE) System.out.print("\n");
											}
											else if(technique.getPhong().getSpecular().getTexture() != null){
												String paramName = technique.getPhong().getSpecular().getTexture().getTexture();
												for(Object inObject : profileCommon.getImageOrNewparam()){
													if(inObject instanceof CommonNewparamType){
														if(((CommonNewparamType)inObject).getSid().equals(paramName)){
															fileOut.writeInt(API.SAMPLER);
															final FxSampler2DCommon sampler = ((CommonNewparamType)inObject).getSampler2D();
															String surfaceId = null;
															if(sampler != null){
																for(Object inObject2 : profileCommon.getImageOrNewparam()){
																	if(inObject2 instanceof CommonNewparamType){
																		if(((CommonNewparamType)inObject2).getSid().equals(sampler.getSource())){
																			if(((CommonNewparamType)inObject2).getSurface() != null){
																				surfaceId = ((Image)((CommonNewparamType)inObject2).getSurface().getInitFrom().get(0).getValue()).getId();
																			}
																		}
																	}
																}
																if(surfaceId != null){
																	if(MODE_VERBOSE) System.out.print(URIToID(surfaceId));
																	fileOut.writeInt(URIToID(surfaceId));
																	if(MODE_VERBOSE) System.out.println(" -> Semantic : "+URIToID(technique.getPhong().getSpecular().getTexture().getTexcoord()));
																	fileOut.writeInt(URIToID(technique.getPhong().getSpecular().getTexture().getTexcoord()));
																}
																else{
																	fileOut.writeInt(API.UNSPECIFIED);
																	errorCount++;
																	System.err.println("\n            [ERROR] Surface ID not found for sampler "+paramName);
																}
															}
															else{
																fileOut.writeInt(API.UNSPECIFIED);
																errorCount++;
																System.err.println("\n            [ERROR] Sampler not found "+paramName);
															}
														}
													}
												}
											}
											else if(technique.getPhong().getSpecular().getParam() != null){
												errorCount++;
												System.err.println("\n            [ERROR] Params not supported in effect "+sourceEffect.getId());
											}
										}
										else{
											fileOut.writeInt(API.UNSPECIFIED);
											if(MODE_VERBOSE) System.out.println("not set");
										}
										if(MODE_VERBOSE) System.out.print("                            > Shininess : ");
										if(technique.getPhong().getShininess() != null){
											if(technique.getPhong().getShininess().getFloat() != null){
												fileOut.writeInt(API.FLOAT);
												fileOut.writeFloat((float)technique.getPhong().getShininess().getFloat().getValue());
												if(MODE_VERBOSE) System.out.println((float)technique.getPhong().getShininess().getFloat().getValue());
											}
											else if(technique.getPhong().getShininess().getParam() != null){
												errorCount++;
												System.err.println("\n            [ERROR] Params not supported in effect "+sourceEffect.getId());
											}
										}
										else{
											fileOut.writeInt(API.UNSPECIFIED);
											if(MODE_VERBOSE) System.out.println("not set");
										}
									}
									//BLINN MATERIAL
									else if(technique.getBlinn() != null){
										mType = API.SPECULAR;
										fileOut.writeInt(mType);
										
										if(MODE_VERBOSE) System.out.println("                        > Type : SPECULAR");
										if(MODE_VERBOSE) System.out.print("                            > Emission : ");
										if(technique.getBlinn().getEmission() != null){
											if(technique.getBlinn().getEmission().getColor() != null){
												List<Double> colorValues = technique.getBlinn().getEmission().getColor().getValue();
												fileOut.writeInt(API.FLOAT4);
												for(Double colorValue : colorValues){
													if(MODE_VERBOSE) System.out.print(colorValue+" ");
													fileOut.writeFloat(colorValue.floatValue());
												}
												if(colorValues.size() == 3){
													fileOut.writeFloat(1.0f);
												}
												if(MODE_VERBOSE) System.out.print("\n");
											}
											else if(technique.getBlinn().getEmission().getTexture() != null){
												String paramName = technique.getBlinn().getEmission().getTexture().getTexture();
												for(Object inObject : profileCommon.getImageOrNewparam()){
													if(inObject instanceof CommonNewparamType){
														if(((CommonNewparamType)inObject).getSid().equals(paramName)){
															fileOut.writeInt(API.SAMPLER);
															final FxSampler2DCommon sampler = ((CommonNewparamType)inObject).getSampler2D();
															String surfaceId = null;
															if(sampler != null){
																for(Object inObject2 : profileCommon.getImageOrNewparam()){
																	if(inObject2 instanceof CommonNewparamType){
																		if(((CommonNewparamType)inObject2).getSid().equals(sampler.getSource())){
																			if(((CommonNewparamType)inObject2).getSurface() != null){
																				surfaceId = ((Image)((CommonNewparamType)inObject2).getSurface().getInitFrom().get(0).getValue()).getId();
																			}
																		}
																	}
																}
																if(surfaceId != null){
																	if(MODE_VERBOSE) System.out.print(URIToID(surfaceId));
																	fileOut.writeInt(URIToID(surfaceId));
																	if(MODE_VERBOSE) System.out.println(" -> Semantic : "+URIToID(technique.getBlinn().getEmission().getTexture().getTexcoord()));
																	fileOut.writeInt(URIToID(technique.getBlinn().getEmission().getTexture().getTexcoord()));
																}
																else{
																	fileOut.writeInt(API.UNSPECIFIED);
																	errorCount++;
																	System.err.println("\n            [ERROR] Surface ID not found for sampler "+paramName);
																}
															}
															else{
																fileOut.writeInt(API.UNSPECIFIED);
																errorCount++;
																System.err.println("\n            [ERROR] Sampler not found "+paramName);
															}
														}
													}
												}
											}
											else if(technique.getBlinn().getEmission().getParam() != null){
												errorCount++;
												System.err.println("\n            [ERROR] Params not supported in effect "+sourceEffect.getId());
											}
										}
										else{
											fileOut.writeInt(API.UNSPECIFIED);
											if(MODE_VERBOSE) System.out.println("not set");
										}
										if(MODE_VERBOSE) System.out.print("                            > Reflective : ");
										if(technique.getBlinn().getReflective() != null){
											if(technique.getBlinn().getReflective().getColor() != null){
												List<Double> colorValues = technique.getBlinn().getReflective().getColor().getValue();
												fileOut.writeInt(API.FLOAT4);
												for(Double colorValue : colorValues){
													if(MODE_VERBOSE) System.out.print(colorValue+" ");
													fileOut.writeFloat(colorValue.floatValue());
												}
												if(colorValues.size() == 3){
													fileOut.writeFloat(1.0f);
												}
												if(MODE_VERBOSE) System.out.print("\n");
											}
											else if(technique.getBlinn().getReflective().getTexture() != null){
												String paramName = technique.getBlinn().getReflective().getTexture().getTexture();
												for(Object inObject : profileCommon.getImageOrNewparam()){
													if(inObject instanceof CommonNewparamType){
														if(((CommonNewparamType)inObject).getSid().equals(paramName)){
															fileOut.writeInt(API.SAMPLER);
															final FxSampler2DCommon sampler = ((CommonNewparamType)inObject).getSampler2D();
															String surfaceId = null;
															if(sampler != null){
																for(Object inObject2 : profileCommon.getImageOrNewparam()){
																	if(inObject2 instanceof CommonNewparamType){
																		if(((CommonNewparamType)inObject2).getSid().equals(sampler.getSource())){
																			if(((CommonNewparamType)inObject2).getSurface() != null){
																				surfaceId = ((Image)((CommonNewparamType)inObject2).getSurface().getInitFrom().get(0).getValue()).getId();
																			}
																		}
																	}
																}
																if(surfaceId != null){
																	if(MODE_VERBOSE) System.out.print(URIToID(surfaceId));
																	fileOut.writeInt(URIToID(surfaceId));
																	if(MODE_VERBOSE) System.out.println(" -> Semantic : "+URIToID(technique.getBlinn().getReflective().getTexture().getTexcoord()));
																	fileOut.writeInt(URIToID(technique.getBlinn().getReflective().getTexture().getTexcoord()));
																}
																else{
																	fileOut.writeInt(API.UNSPECIFIED);
																	errorCount++;
																	System.err.println("\n            [ERROR] Surface ID not found for sampler "+paramName);
																}
															}
															else{
																fileOut.writeInt(API.UNSPECIFIED);
																errorCount++;
																System.err.println("\n            [ERROR] Sampler not found "+paramName);
															}
														}
													}
												}
											}
											else if(technique.getBlinn().getReflective().getParam() != null){
												errorCount++;
												System.err.println("\n            [ERROR] Params not supported in effect "+sourceEffect.getId());
											}
										}
										else{
											fileOut.writeInt(API.UNSPECIFIED);
											if(MODE_VERBOSE) System.out.println("not set");
										}
										if(MODE_VERBOSE) System.out.print("                            > Reflectivity : ");
										if(technique.getBlinn().getReflectivity() != null){
											if(technique.getBlinn().getReflectivity().getFloat() != null){
												fileOut.writeInt(API.FLOAT);
												fileOut.writeFloat((float)technique.getBlinn().getReflectivity().getFloat().getValue());
												if(MODE_VERBOSE) System.out.println((float)technique.getBlinn().getReflectivity().getFloat().getValue());
											}
											else if(technique.getBlinn().getReflectivity().getParam() != null){
												errorCount++;
												System.err.println("\n            [ERROR] Params not supported in effect "+sourceEffect.getId());
											}
										}
										else{
											fileOut.writeInt(API.UNSPECIFIED);
											if(MODE_VERBOSE) System.out.println("not set");
										}
										if(MODE_VERBOSE) System.out.print("                            > Transparent : ");
										if(technique.getBlinn().getTransparent() != null){
											if(technique.getBlinn().getTransparent().getColor() != null){
												List<Double> colorValues = technique.getBlinn().getTransparent().getColor().getValue();
												fileOut.writeInt(API.FLOAT4);
												for(Double colorValue : colorValues){
													if(MODE_VERBOSE) System.out.print(colorValue+" ");
													fileOut.writeFloat(colorValue.floatValue());
												}
												if(colorValues.size() == 3){
													fileOut.writeFloat(1.0f);
												}
												if(MODE_VERBOSE) System.out.print("\n");
											}
											else if(technique.getBlinn().getTransparent().getTexture() != null){
												String paramName = technique.getBlinn().getTransparent().getTexture().getTexture();
												for(Object inObject : profileCommon.getImageOrNewparam()){
													if(inObject instanceof CommonNewparamType){
														if(((CommonNewparamType)inObject).getSid().equals(paramName)){
															fileOut.writeInt(API.SAMPLER);
															final FxSampler2DCommon sampler = ((CommonNewparamType)inObject).getSampler2D();
															String surfaceId = null;
															if(sampler != null){
																for(Object inObject2 : profileCommon.getImageOrNewparam()){
																	if(inObject2 instanceof CommonNewparamType){
																		if(((CommonNewparamType)inObject2).getSid().equals(sampler.getSource())){
																			if(((CommonNewparamType)inObject2).getSurface() != null){
																				surfaceId = ((Image)((CommonNewparamType)inObject2).getSurface().getInitFrom().get(0).getValue()).getId();
																			}
																		}
																	}
																}
																if(surfaceId != null){
																	if(MODE_VERBOSE) System.out.print(URIToID(surfaceId));
																	fileOut.writeInt(URIToID(surfaceId));
																	if(MODE_VERBOSE) System.out.println(" -> Semantic : "+URIToID(technique.getBlinn().getTransparent().getTexture().getTexcoord()));
																	fileOut.writeInt(URIToID(technique.getBlinn().getTransparent().getTexture().getTexcoord()));
																}
																else{
																	fileOut.writeInt(API.UNSPECIFIED);
																	errorCount++;
																	System.err.println("\n            [ERROR] Surface ID not found for sampler "+paramName);
																}
															}
															else{
																fileOut.writeInt(API.UNSPECIFIED);
																errorCount++;
																System.err.println("\n            [ERROR] Sampler not found "+paramName);
															}
														}
													}
												}
											}
											else if(technique.getBlinn().getTransparent().getParam() != null){
												errorCount++;
												System.err.println("\n            [ERROR] Params not supported in effect "+sourceEffect.getId());
											}
										}
										else{
											fileOut.writeInt(API.UNSPECIFIED);
											if(MODE_VERBOSE) System.out.println("not set");
										}
										if(MODE_VERBOSE) System.out.print("                            > Transparency : ");
										if(technique.getBlinn().getTransparency() != null){
											if(technique.getBlinn().getTransparency().getFloat() != null){
												fileOut.writeInt(API.FLOAT);
												fileOut.writeFloat((float)technique.getBlinn().getTransparency().getFloat().getValue());
												if(MODE_VERBOSE) System.out.println((float)technique.getBlinn().getTransparency().getFloat().getValue());
											}
											else if(technique.getBlinn().getTransparency().getParam() != null){
												errorCount++;
												System.err.println("\n            [ERROR] Params not supported in effect "+sourceEffect.getId());
											}
										}
										else{
											fileOut.writeInt(API.UNSPECIFIED);
											if(MODE_VERBOSE) System.out.println("not set");
										}
										if(MODE_VERBOSE) System.out.print("                            > Refraction : ");
										if(technique.getBlinn().getIndexOfRefraction() != null){
											if(technique.getBlinn().getIndexOfRefraction().getFloat() != null){
												fileOut.writeInt(API.FLOAT);
												fileOut.writeFloat((float)technique.getBlinn().getIndexOfRefraction().getFloat().getValue());
												if(MODE_VERBOSE) System.out.println((float)technique.getBlinn().getIndexOfRefraction().getFloat().getValue());
											}
											else if(technique.getBlinn().getIndexOfRefraction().getParam() != null){
												errorCount++;
												System.err.println("\n            [ERROR] Params not supported in effect "+sourceEffect.getId());
											}
										}
										else{
											fileOut.writeInt(API.UNSPECIFIED);
											if(MODE_VERBOSE) System.out.println("not set");
										}
										if(MODE_VERBOSE) System.out.print("                            > Ambient : ");
										if(technique.getBlinn().getAmbient() != null){
											if(technique.getBlinn().getAmbient().getColor() != null){
												List<Double> colorValues = technique.getBlinn().getAmbient().getColor().getValue();
												fileOut.writeInt(API.FLOAT4);
												for(Double colorValue : colorValues){
													if(MODE_VERBOSE) System.out.print(colorValue+" ");
													fileOut.writeFloat(colorValue.floatValue());
												}
												if(colorValues.size() == 3){
													fileOut.writeFloat(1.0f);
												}
												if(MODE_VERBOSE) System.out.print("\n");
											}
											else if(technique.getBlinn().getAmbient().getTexture() != null){
												String paramName = technique.getBlinn().getAmbient().getTexture().getTexture();
												for(Object inObject : profileCommon.getImageOrNewparam()){
													if(inObject instanceof CommonNewparamType){
														if(((CommonNewparamType)inObject).getSid().equals(paramName)){
															fileOut.writeInt(API.SAMPLER);
															final FxSampler2DCommon sampler = ((CommonNewparamType)inObject).getSampler2D();
															String surfaceId = null;
															if(sampler != null){
																for(Object inObject2 : profileCommon.getImageOrNewparam()){
																	if(inObject2 instanceof CommonNewparamType){
																		if(((CommonNewparamType)inObject2).getSid().equals(sampler.getSource())){
																			if(((CommonNewparamType)inObject2).getSurface() != null){
																				surfaceId = ((Image)((CommonNewparamType)inObject2).getSurface().getInitFrom().get(0).getValue()).getId();
																			}
																		}
																	}
																}
																if(surfaceId != null){
																	if(MODE_VERBOSE) System.out.print(URIToID(surfaceId));
																	fileOut.writeInt(URIToID(surfaceId));
																	if(MODE_VERBOSE) System.out.println(" -> Semantic : "+URIToID(technique.getBlinn().getAmbient().getTexture().getTexcoord()));
																	fileOut.writeInt(URIToID(technique.getBlinn().getAmbient().getTexture().getTexcoord()));
																}
																else{
																	fileOut.writeInt(API.UNSPECIFIED);
																	errorCount++;
																	System.err.println("\n            [ERROR] Surface ID not found for sampler "+paramName);
																}
															}
															else{
																fileOut.writeInt(API.UNSPECIFIED);
																errorCount++;
																System.err.println("\n            [ERROR] Sampler not found "+paramName);
															}
														}
													}
												}
											}
											else if(technique.getBlinn().getAmbient().getParam() != null){
												errorCount++;
												System.err.println("\n            [ERROR] Params not supported in effect "+sourceEffect.getId());
											}
										}
										else{
											fileOut.writeInt(API.UNSPECIFIED);
											if(MODE_VERBOSE) System.out.println("not set");
										}
										if(MODE_VERBOSE) System.out.print("                            > Diffuse : ");
										if(technique.getBlinn().getDiffuse() != null){
											if(technique.getBlinn().getDiffuse().getColor() != null){
												List<Double> colorValues = technique.getBlinn().getDiffuse().getColor().getValue();
												fileOut.writeInt(API.FLOAT4);
												for(Double colorValue : colorValues){
													if(MODE_VERBOSE) System.out.print(colorValue+" ");
													fileOut.writeFloat(colorValue.floatValue());
												}
												if(colorValues.size() == 3){
													fileOut.writeFloat(1.0f);
												}
												if(MODE_VERBOSE) System.out.print("\n");
											}
											else if(technique.getBlinn().getDiffuse().getTexture() != null){
												String paramName = technique.getBlinn().getDiffuse().getTexture().getTexture();
												for(Object inObject : profileCommon.getImageOrNewparam()){
													if(inObject instanceof CommonNewparamType){
														if(((CommonNewparamType)inObject).getSid().equals(paramName)){
															fileOut.writeInt(API.SAMPLER);
															final FxSampler2DCommon sampler = ((CommonNewparamType)inObject).getSampler2D();
															String surfaceId = null;
															if(sampler != null){
																for(Object inObject2 : profileCommon.getImageOrNewparam()){
																	if(inObject2 instanceof CommonNewparamType){
																		if(((CommonNewparamType)inObject2).getSid().equals(sampler.getSource())){
																			if(((CommonNewparamType)inObject2).getSurface() != null){
																				surfaceId = ((Image)((CommonNewparamType)inObject2).getSurface().getInitFrom().get(0).getValue()).getId();
																			}
																		}
																	}
																}
																if(surfaceId != null){
																	if(MODE_VERBOSE) System.out.print(URIToID(surfaceId));
																	fileOut.writeInt(URIToID(surfaceId));
																	if(MODE_VERBOSE) System.out.println(" -> Semantic : "+URIToID(technique.getBlinn().getDiffuse().getTexture().getTexcoord()));
																	fileOut.writeInt(URIToID(technique.getBlinn().getDiffuse().getTexture().getTexcoord()));
																}
																else{
																	fileOut.writeInt(API.UNSPECIFIED);
																	errorCount++;
																	System.err.println("\n            [ERROR] Surface ID not found for sampler "+paramName);
																}
															}
															else{
																fileOut.writeInt(API.UNSPECIFIED);
																errorCount++;
																System.err.println("\n            [ERROR] Sampler not found "+paramName);
															}
														}
													}
												}
											}
											else if(technique.getBlinn().getDiffuse().getParam() != null){
												errorCount++;
												System.err.println("\n            [ERROR] Params not supported in effect "+sourceEffect.getId());
											}
										}
										else{
											fileOut.writeInt(API.UNSPECIFIED);
											if(MODE_VERBOSE) System.out.println("not set");
										}
										if(MODE_VERBOSE) System.out.print("                            > Specular : ");
										if(technique.getBlinn().getSpecular() != null){
											if(technique.getBlinn().getSpecular().getColor() != null){
												List<Double> colorValues = technique.getBlinn().getSpecular().getColor().getValue();
												fileOut.writeInt(API.FLOAT4);
												for(Double colorValue : colorValues){
													if(MODE_VERBOSE) System.out.print(colorValue+" ");
													fileOut.writeFloat(colorValue.floatValue());
												}
												if(colorValues.size() == 3){
													fileOut.writeFloat(1.0f);
												}
												if(MODE_VERBOSE) System.out.print("\n");
											}
											else if(technique.getBlinn().getSpecular().getTexture() != null){
												String paramName = technique.getBlinn().getSpecular().getTexture().getTexture();
												for(Object inObject : profileCommon.getImageOrNewparam()){
													if(inObject instanceof CommonNewparamType){
														if(((CommonNewparamType)inObject).getSid().equals(paramName)){
															fileOut.writeInt(API.SAMPLER);
															final FxSampler2DCommon sampler = ((CommonNewparamType)inObject).getSampler2D();
															String surfaceId = null;
															if(sampler != null){
																for(Object inObject2 : profileCommon.getImageOrNewparam()){
																	if(inObject2 instanceof CommonNewparamType){
																		if(((CommonNewparamType)inObject2).getSid().equals(sampler.getSource())){
																			if(((CommonNewparamType)inObject2).getSurface() != null){
																				surfaceId = ((Image)((CommonNewparamType)inObject2).getSurface().getInitFrom().get(0).getValue()).getId();
																			}
																		}
																	}
																}
																if(surfaceId != null){
																	if(MODE_VERBOSE) System.out.print(URIToID(surfaceId));
																	fileOut.writeInt(URIToID(surfaceId));
																	if(MODE_VERBOSE) System.out.println(" -> Semantic : "+URIToID(technique.getBlinn().getSpecular().getTexture().getTexcoord()));
																	fileOut.writeInt(URIToID(technique.getBlinn().getSpecular().getTexture().getTexcoord()));
																}
																else{
																	fileOut.writeInt(API.UNSPECIFIED);
																	errorCount++;
																	System.err.println("\n            [ERROR] Surface ID not found for sampler "+paramName);
																}
															}
															else{
																fileOut.writeInt(API.UNSPECIFIED);
																errorCount++;
																System.err.println("\n            [ERROR] Sampler not found "+paramName);
															}
														}
													}
												}
											}
											else if(technique.getBlinn().getSpecular().getParam() != null){
												errorCount++;
												System.err.println("\n            [ERROR] Params not supported in effect "+sourceEffect.getId());
											}
										}
										else{
											fileOut.writeInt(API.UNSPECIFIED);
											if(MODE_VERBOSE) System.out.println("not set");
										}
										if(MODE_VERBOSE) System.out.print("                            > Shininess : ");
										if(technique.getBlinn().getShininess() != null){
											if(technique.getBlinn().getShininess().getFloat() != null){
												fileOut.writeInt(API.FLOAT);
												fileOut.writeFloat((float)technique.getBlinn().getShininess().getFloat().getValue());
												if(MODE_VERBOSE) System.out.println((float)technique.getBlinn().getShininess().getFloat().getValue());
											}
											else if(technique.getBlinn().getShininess().getParam() != null){
												errorCount++;
												System.err.println("\n            [ERROR] Params not supported in effect "+sourceEffect.getId());
											}
										}
										else{
											fileOut.writeInt(API.UNSPECIFIED);
											if(MODE_VERBOSE) System.out.println("not set");
										}
									}
									else{
										errorCount++;
										System.err.println("            [ERROR] No shader found in profile of effect "+sourceEffect.getId());
									}
								}
								else{
									errorCount++;
									System.err.println("            [ERROR] No technique found in profile of effect "+sourceEffect.getId());
								}
							}
							else{
								errorCount++;
								System.err.println("            [ERROR] No common profile found for effect "+sourceEffect.getId());
							}
						}
						else{
							errorCount++;
							System.err.println("            [ERROR] No effect found for this material");
						}
						
						materialCount++;
					}
					
					if(materialCount == 0){
						System.err.println("            > No material found");
					}
				}
			}
			/************************************************************************************************
			 * NODE - Describes the nodes element in the scene	
			 ************************************************************************************************
			  
			  [NODE][$ID][$NODE_TYPE][$ELEMENT_COUNT]
			  		if LOOKAT : [LOOKAT][$EYE_X][$EYE_Y][$EYE_Z][$INTEREST_X][$INTEREST_Y][$INTEREST_Z][$UP_X][$UP_Y][$UP_Z]
			  		if MATRIX : [MATRIX][$0_0]...[$3_3]
			  		if ROTATE : [ROTATE][$ROTATE_X][$ROTATE_Y][$ROTATE_Z][$ROTATE_DEGRE]
			  		if SCALE : [SCALE][$SCALE_X][$SCALE_Y][$SCALE_Z]
			  		if SKEW : [SKEW][$ROTATE_DEGRE][$ROTATE_X][$ROTATE_Y][$ROTATE_Z][$TRANSLATE_X][$TRANSLATE_Y][$TRANSLATE_Z]
			  		if TRANSLATE : [TRANSLATE][$TRANSLATE_X][$TRANSLATE_Y][$TRANSLATE_Z]
			  		if CAMERA : [CAMERA][$CAMERA_ID]
			  		if GEOMETRY : [GEOMETRY][$GEOMETRY_ID][$MATERIAL_COUNT]
			  			[$MATERIAL_ID][$MATERIAL_TARGET_ID][$BIND_COUNT]
			  				[$INPUT_SEMANTIC][$INPUT_SET][$MATERIAL_SEMANTIC]
			  				...
			  				[$INPUT_SEMANTIC][$INPUT_SET][$MATERIAL_SEMANTIC]
			  			...
			  			[$MATERIAL_ID][$MATERIAL_TARGET_ID][$BIND_COUNT]
			  		if LIGHT : [LIGHT][$LIGHT_ID]
			  		if NODE : [NODE][$NODE_ID]
			  ...
			  [NODE][$ID][$NODE_TYPE][$ELEMENT_COUNT]
			 ************************************************************************************************/
			if(MODE_VERBOSE) System.out.println("    > Node Elements");
			apiWriter.write("\n\t//LibraryNodes\n");
			for(Node node : allNodes){
				
				int NODE_ID = node.getId().hashCode();
				
				if(MODE_VERBOSE) System.out.println("            > NODE : "+node.getId());
				fileOut.writeInt(API.NODE);
				fileOut.writeInt(NODE_ID);
				
				if(node.getType().compareTo(NodeType.JOINT) == 0){
					fileOut.writeInt(API.JOINT);
					if(MODE_VERBOSE) System.out.println("                > type : JOINT");
				}
				else{
					fileOut.writeInt(API.NODE);
					if(MODE_VERBOSE) System.out.println("                > type : NODE");
				}
				
				int ELEMENT_COUNT = node.getLookatOrMatrixOrRotate().size()
										+ node.getInstanceCamera().size()
										+ node.getInstanceGeometry().size()
										+ node.getInstanceLight().size()
										+ node.getInstanceNode().size()
										+ node.getNode().size();
				
				fileOut.writeInt(ELEMENT_COUNT);
				
				apiWriter.write("\tpublic static final int NODE_"+IDtoConst(node.getId())+" = "+NODE_ID+";\n");
				
				if(MODE_VERBOSE) System.out.println("                > id : "+NODE_ID);
				if(MODE_VERBOSE) System.out.println("                > elements : "+ELEMENT_COUNT);
				
				for(Object object : node.getLookatOrMatrixOrRotate()){
					//LOOKAT : [LOOKAT][$EYE_X][$EYE_Y][$EYE_Z][$INTEREST_X][$INTEREST_Y][$INTEREST_Z][$UP_X][$UP_Y][$UP_Z]
					if(object instanceof Lookat){
						fileOut.writeInt(API.LOOKAT);
						if(MODE_VERBOSE) System.out.print("                > LOOKAT : ");
						for(Double value : ((Lookat)object).getValue()){
							fileOut.writeFloat(value.floatValue());
							if(MODE_VERBOSE) System.out.print(value.floatValue() + " ");
						}
						if(MODE_VERBOSE) System.out.print("\n");
					}
					//MATRIX : [MATRIX][$0_0]...[$3_3]
					else if(object instanceof Matrix){
						fileOut.writeInt(API.MATRIX);
						if(MODE_VERBOSE) System.out.print("                > MATRIX : ");
						for(Double value : ((Matrix)object).getValue()){
							fileOut.writeFloat(value.floatValue());
							if(MODE_VERBOSE) System.out.print(value.floatValue() + " ");
						}
						if(MODE_VERBOSE) System.out.print("\n");
					}
					//ROTATE : [ROTATE][$ROTATE_X][$ROTATE_Y][$ROTATE_Z][$ROTATE_DEGRE]
					else if(object instanceof Rotate){
						fileOut.writeInt(API.ROTATE);
						if(MODE_VERBOSE) System.out.print("                > ROTATE : ");								
						for(Double value : ((Rotate)object).getValue()){
							fileOut.writeFloat(value.floatValue());
							if(MODE_VERBOSE) System.out.print(value.floatValue() + " ");
						}
						if(MODE_VERBOSE) System.out.print("\n");
					}
					//SKEW : [SKEW][$ROTATE_DEGRE][$ROTATE_X][$ROTATE_Y][$ROTATE_Z][$TRANSLATE_X][$TRANSLATE_Y][$TRANSLATE_Z]
					else if(object instanceof Skew){
						fileOut.writeInt(API.SKEW);
						if(MODE_VERBOSE) System.out.print("                > SKEW : ");
						for(Double value : ((Skew)object).getValue()){
							fileOut.writeFloat(value.floatValue());
							if(MODE_VERBOSE) System.out.print(value.floatValue() + " ");
						}
						if(MODE_VERBOSE) System.out.print("\n");
					}
					else if(object instanceof JAXBElement){
						//SCALE : [SCALE][$SCALE_X][$SCALE_Y][$SCALE_Z]
						if(((JAXBElement)object).getName().getLocalPart().equals("scale")){
							fileOut.writeInt(API.SCALE);
							if(MODE_VERBOSE) System.out.print("                > SCALE : ");
						}
						//TRANSLATE : [TRANSLATE][$TRANSLATE_X][$TRANSLATE_Y][$TRANSLATE_Z]
						else if(((JAXBElement)object).getName().getLocalPart().equals("translate")){
							fileOut.writeInt(API.TRANSLATE);
							if(MODE_VERBOSE) System.out.print("                > TRANSLATE : ");
						}
						for(Double value : ((TargetableFloat3)((JAXBElement)object).getValue()).getValue()){
							fileOut.writeFloat(value.floatValue());
							if(MODE_VERBOSE) System.out.print(value.floatValue() + " ");
						}
						if(MODE_VERBOSE) System.out.print("\n");
					}
				}
				
				// CAMERA : [CAMERA][$CAMERA_ID]
				for(InstanceWithExtra camera : node.getInstanceCamera()){
					int CAMERA_ID = URIToID(camera.getUrl());
					fileOut.writeInt(API.CAMERA);
					fileOut.writeInt(CAMERA_ID);
					if(MODE_VERBOSE) System.out.println("                > CAMERA : "+CAMERA_ID);
				}
				
				// GEOMETRY : [GEOMETRY][$GEOMETRY_ID][$MATERIAL_COUNT]
				for(InstanceGeometry geometry : node.getInstanceGeometry()){
					int GEOMETRY_ID = URIToID(geometry.getUrl());
					int MATERIAL_COUNT = (geometry.getBindMaterial() == null)? 0 : geometry.getBindMaterial().getTechniqueCommon().getInstanceMaterial().size();
					fileOut.writeInt(API.GEOMETRY);
					fileOut.writeInt(GEOMETRY_ID);
					fileOut.writeInt(MATERIAL_COUNT);
					if(MODE_VERBOSE) System.out.println("                > GEOMETRY : "+GEOMETRY_ID + " ["+MATERIAL_COUNT+" materials]");
						
					if(geometry.getBindMaterial() != null){
						// [$MATERIAL_ID][$MATERIAL_TARGET_ID][$BIND_COUNT]
						for(InstanceMaterial material : geometry.getBindMaterial().getTechniqueCommon().getInstanceMaterial()){
							int MATERIAL_ID = URIToID(material.getSymbol());
							int TARGET_ID = URIToID(material.getTarget());
							int BIND_COUNT = material.getBindVertexInput().size();
							
							fileOut.writeInt(MATERIAL_ID);
							fileOut.writeInt(TARGET_ID);
							fileOut.writeInt(BIND_COUNT);
							if(MODE_VERBOSE) System.out.println("                    > MATERIAL : "+MATERIAL_ID + " on "+TARGET_ID+" ["+BIND_COUNT+" binds]");
							
							// [$INPUT_SEMANTIC][$INPUT_SET][$MATERIAL_SEMANTIC]
							for(BindVertexInput bind : material.getBindVertexInput()){
								int INPUT_SEMANTIC = API.UNSPECIFIED;
								if(bind.getInputSemantic().startsWith("BINORMAL")) INPUT_SEMANTIC = API.BINORMAL;
								else if(bind.getInputSemantic().startsWith("COLOR")) INPUT_SEMANTIC = API.COLOR;
								else if(bind.getInputSemantic().startsWith("CONTINUITY")) INPUT_SEMANTIC = API.CONTINUITY;
								else if(bind.getInputSemantic().startsWith("IMAGE")) INPUT_SEMANTIC = API.IMAGE;
								else if(bind.getInputSemantic().startsWith("MATERIAL")) INPUT_SEMANTIC = API.MATERIAL;
								else if(bind.getInputSemantic().startsWith("IN_TANGENT")) INPUT_SEMANTIC = API.IN_TANGENT;
								else if(bind.getInputSemantic().startsWith("INTERPOLATION")) INPUT_SEMANTIC = API.INTERPOLATION;
								else if(bind.getInputSemantic().startsWith("INV_BIND_MATRIX")) INPUT_SEMANTIC = API.INV_BIND_MATRIX;
								else if(bind.getInputSemantic().startsWith("JOINT")) INPUT_SEMANTIC = API.JOINT;
								else if(bind.getInputSemantic().startsWith("LINEAR_STEPS")) INPUT_SEMANTIC = API.LINEAR_STEPS;
								else if(bind.getInputSemantic().startsWith("MORPH_TARGET")) INPUT_SEMANTIC = API.MORPH_TARGET;
								else if(bind.getInputSemantic().startsWith("MORPH_WEIGHT")) INPUT_SEMANTIC = API.MORPH_WEIGHT;
								else if(bind.getInputSemantic().startsWith("NORMAL")) INPUT_SEMANTIC = API.NORMAL;
								else if(bind.getInputSemantic().startsWith("OUTPUT")) INPUT_SEMANTIC = API.OUTPUT;
								else if(bind.getInputSemantic().startsWith("OUT_TANGENT")) INPUT_SEMANTIC = API.OUT_TANGENT;
								else if(bind.getInputSemantic().startsWith("POSITION")) INPUT_SEMANTIC = API.POSITION;
								else if(bind.getInputSemantic().startsWith("TANGENT")) INPUT_SEMANTIC = API.TANGENT;
								else if(bind.getInputSemantic().startsWith("TEXBINORMAL")) INPUT_SEMANTIC = API.TEXBINORMAL;
								else if(bind.getInputSemantic().startsWith("TEXCOORD")) INPUT_SEMANTIC = API.TEXCOORD;
								else if(bind.getInputSemantic().startsWith("TEXTANGENT")) INPUT_SEMANTIC = API.TEXTANGENT;
								else if(bind.getInputSemantic().startsWith("UV")) INPUT_SEMANTIC = API.UV;
								else if(bind.getInputSemantic().startsWith("VERTEX")) INPUT_SEMANTIC = API.VERTEX;
								else if(bind.getInputSemantic().startsWith("WEIGHT")) INPUT_SEMANTIC = API.WEIGHT;										
								int INPUT_SET = bind.getInputSet().intValue();
								int MATERIAL_SEMANTIC = URIToID(bind.getSemantic());
								
								fileOut.writeInt(INPUT_SEMANTIC);
								fileOut.writeInt(INPUT_SET);
								fileOut.writeInt(MATERIAL_SEMANTIC);
								if(MODE_VERBOSE) System.out.println("                        > BIND : "+MATERIAL_SEMANTIC+" on "+INPUT_SEMANTIC + " - set : "+INPUT_SET);
							}
						}
					}
				}
				
				// LIGHT : [LIGHT][$LIGHT_ID]
				for(InstanceWithExtra light : node.getInstanceLight()){
					int LIGHT_ID = URIToID(light.getUrl());
					fileOut.writeInt(API.LIGHT);
					fileOut.writeInt(LIGHT_ID);
					if(MODE_VERBOSE) System.out.println("                > LIGHT : "+LIGHT_ID);
				}
				
				// [NODE][$NODE_TYPE][$ID][$ELEMENT_COUNT] ...
				for(InstanceWithExtra inNode : node.getInstanceNode()){
					int IN_NODE_ID = URIToID(inNode.getUrl());
					fileOut.writeInt(API.NODE);
					fileOut.writeInt(IN_NODE_ID);
					if(MODE_VERBOSE) System.out.println("                > NODE : "+IN_NODE_ID);
				}
				
				// [NODE][$NODE_TYPE][$ID][$ELEMENT_COUNT] ...
				for(Node inNode : node.getNode()){
					int IN_NODE_ID = URIToID(inNode.getId());
					fileOut.writeInt(API.NODE);
					fileOut.writeInt(IN_NODE_ID);
					if(MODE_VERBOSE) System.out.println("                > NODE : "+IN_NODE_ID);
				}
			}
			
			for(Object element : collada.getLibraryAnimationsOrLibraryAnimationClipsOrLibraryCameras()){
				/************************************************************************************************
				 * SCENE - Describes the scene elements	
				 ************************************************************************************************
				  
				  [SCENE][$ID][$NODE_COUNT]
				  [NODE_ID]
				  ...		
				  [SCENE][$ID][$NODE_COUNT]
				 ************************************************************************************************/
				if(element instanceof LibraryVisualScenes){
					if(MODE_VERBOSE) System.out.println("    > Scene Elements");
					apiWriter.write("\n\t//LibraryVisualScenes\n");
					for(VisualScene scene : ((LibraryVisualScenes)element).getVisualScene()){
						if(MODE_VERBOSE) System.out.println("            > Parsing "+scene.getId());
						//[SCENE][$ID][$NODE_COUNT]
						int ID = scene.getId().hashCode();
						int NODE_COUNT = scene.getNode().size();
							
						fileOut.writeInt(API.SCENE);
						fileOut.writeInt(ID);
						fileOut.writeInt(NODE_COUNT);
						
						apiWriter.write("\tpublic static final int SCENE_"+IDtoConst(scene.getId())+" = "+ID+";\n");
						
						if(MODE_VERBOSE) System.out.println("                > id : "+ID);
						if(MODE_VERBOSE) System.out.print("                > nodes : "+NODE_COUNT+ " -> ");
						
						//[NODE_ID]
						for(Node node : scene.getNode()){
							fileOut.writeInt(node.getId().hashCode());
							if(MODE_VERBOSE) System.out.print(node.getId().hashCode()+" ");
						}
						if(MODE_VERBOSE) System.out.print("\n");
						sceneCount++;
					}
					if(sceneCount == 0){
						if(MODE_VERBOSE) System.out.println("            > No scene found");
					}
				}
			}
			
			if(errorCount == 0){
				System.out.println(">>>> Errors 0, Warnings "+warningCount+", successful export");
			}
			else{
				System.out.println(">>>> Errors "+errorCount+", Warnings "+warningCount+", check "+inFile.getName());	
			}
			
			apiWriter.write("}");
			
		}finally{
			if(fileOut != null)
				try {
					fileOut.close();
				} catch (IOException e) {
			}
			if(apiWriter != null)
				try {
					apiWriter.close();
				} catch (IOException e) {
			}
		}
			
	}
	
	
	/**
	 * @param Array of files to minfify
	 */
	public static void main(String[] args) {
		COLLADAFilter colladaFilter = null;
		final File outFolder = new File(OUT_FOLDER);
		
		int filesCount = args.length;  
		
		for(int argIndex=0; argIndex < args.length; argIndex++){
			String arg = args[argIndex];
			if(arg.equals("-v") || arg.equals("--verbose")){
				MODE_VERBOSE = true;
				filesCount--;
			}
			else if(arg.equals("-f") || arg.equals("--filter")){
				String filterClassname = args[++argIndex];
				try{
					Class filterClass = COLLADAMinifier.class.getClassLoader().loadClass(filterClassname);
					colladaFilter = (COLLADAFilter)filterClass.newInstance();
				}catch(InstantiationException ie){
					System.err.println("[ERROR] Failed to instanciate filter class : "+filterClassname);
					System.exit(4);
				}catch(IllegalAccessException iae){
					System.err.println("[ERROR] Failed to instanciate filter class : "+filterClassname);
					System.exit(4);
				}catch(ClassNotFoundException cnfe){
					System.err.println("[ERROR] Filter class not found : "+filterClassname);
					System.exit(4);
				}
				filesCount-=2;
			}
		}
		
		if(filesCount <= 0){
			System.err.println("[ERROR] Missing args : COLLADAMinifier [-v] [-f filter_class] input_files");
			System.exit(1);
		}
		
		//Delete old folder
		if(!outFolder.exists()){
			outFolder.mkdirs();
		}
		
		//Processing files
		Unmarshaller unmarshaller;
		try {
			unmarshaller = JAXBContext.newInstance("fr.kesk.libgl.tools.minifier.collada").createUnmarshaller();
		} catch (JAXBException e) {
			unmarshaller = null;
			System.err.println("[ERROR] Failed to initialize JAXB parser");
			System.exit(3);
		}
		
		for(int argIndex=0; argIndex < args.length; argIndex++){
			String arg = args[argIndex];
			if(arg.equals("-v") || arg.equals("--verbose") 
					|| arg.equals("-f") || arg.equals("--filter")){
				continue;
			}
			else{
				File inFile = new File(arg);
				if(inFile.exists()){
					System.out.println("Processing : "+inFile.getName());
					try{
						COLLADA collada = (COLLADA)unmarshaller.unmarshal(inFile);
						
						//Apply tranformations if needed
						if(colladaFilter != null){
							System.out.println("    > Filter : "+colladaFilter.getClass().getSimpleName());
							collada = colladaFilter.parse(collada);
						}
						
						final File outFile = new File(OUT_FOLDER+"/"+inFile.getName().replaceAll("^([^\\.]*).*$", "$1")+".lgl");
						OutputStream outStream = null;
						try{
							if(outFile.exists()){
								outFile.delete();
							}
							
							outStream = new FileOutputStream(outFile);
							parse(collada, inFile, outStream);
							
						}catch(Exception e){	
							if(MODE_VERBOSE) System.err.println("    [ERROR] "+e.getMessage());
							e.printStackTrace();
						}finally{
							if(outStream != null){
								try {
									outStream.close();
								} catch (IOException e) {}
							}
						}
					}catch(JAXBException je){
						System.err.println("    [ERROR] Failed to parse file : "+inFile.getPath());
						je.printStackTrace();
					}
				}
				else{
					System.err.println("    [ERROR] File not found : "+inFile.getPath());
				}
			}
		}
		
	}

}
