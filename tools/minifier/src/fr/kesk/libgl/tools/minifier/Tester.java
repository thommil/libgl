package fr.kesk.libgl.tools.minifier;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;

import fr.kesk.libgl.loader.API;

public class Tester {

	public static boolean MODE_VERBOSE = false;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if(args.length == 0){
			System.err.println("[ERROR] Missing args : Tester [INPUT_FILES]");
			System.exit(1);
		}

		for(String arg : args){
			if(arg.equals("-v")){
				MODE_VERBOSE = true;
			}
			else{
				File inFile = new File(arg);
				if(inFile.exists()){
					System.out.println("Parsing "+inFile.getPath());
					
					
					DataInputStream dataIn = null;
					try{
						long startTime = System.currentTimeMillis();
						FileInputStream fileIn = new FileInputStream(inFile);
						ReadableByteChannel inChannel = Channels.newChannel(fileIn);
						dataIn = new DataInputStream(fileIn);
						final StringBuilder stringBuilder = new StringBuilder();
						
						while(dataIn.available() > 0){
							int entryType = dataIn.readInt();
							switch(entryType){
								//
								// [HEADER][$VERSION]
								// [$GEOMETRY_COUNT][$LIGHT_COUNT][$CAMERA_COUNT][$IMAGERY_COUNT][$MATERIAL_COUNT]							
								case API.HEADER:
									if(MODE_VERBOSE) System.out.println("    > HEADER");
									float hVersion = dataIn.readFloat();
									int hgCount = dataIn.readInt();
									int hlCount = dataIn.readInt();
									int hcCount = dataIn.readInt();
									int hiCount = dataIn.readInt();
									int hmCount = dataIn.readInt();
									int hnCount = dataIn.readInt();
									int hsCount = dataIn.readInt();
									System.out.println("        > version: "+hVersion);
									System.out.println("        > geometries : "+hgCount);
									System.out.println("        > lights: "+hlCount);
									System.out.println("        > cameras: "+hcCount);
									System.out.println("        > imageries: "+hiCount);
									System.out.println("        > materials: "+hmCount);
									System.out.println("        > nodes: "+hnCount);
									System.out.println("        > scenes: "+hsCount);
									break;
								//
								// 	[GEOMETRY][$ID][$PRIMITIVES_COUNT]
								// 	[$PRIMITIVE_TYPE][$INPUT_COUNT][$VERTEX_COUNT][$MATERIAL_ID]
								// 	[$SEMANTIC][$SIZE][$OFFSET]...[$SEMANTIC][$SIZE][$OFFSET]
								// 	[$VALUES]
								//  ...
								//  [$PRIMITIVE_TYPE][$INPUT_COUNT][$VERTEX_COUNT][$MATERIAL_ID]
								//  [$SEMANTIC][$SIZE][$OFFSET]...[$SEMANTIC][$SIZE][$OFFSET]
								//  [$VALUES]
								//  ...
								//  [GEOMETRY][$ID][$PRIMITIVES_COUNT]
								case API.GEOMETRY:
									//[GEOMETRY][$ID][$PRIMITIVES_COUNT]
									if(MODE_VERBOSE) System.out.println("    > GEOMETRY");
									int gId = dataIn.readInt();
									int pCount = dataIn.readInt();
									
									if(MODE_VERBOSE) System.out.println("        > id: "+gId);
									if(MODE_VERBOSE) System.out.println("        > count: "+pCount);
									
									//[$PRIMITIVE_TYPE][$INPUT_COUNT][$VERTEX_COUNT][$MATERIAL_ID]
									for(int pIndex=0; pIndex < pCount; pIndex++){
										if(MODE_VERBOSE) System.out.println("        > PRIMITIVE");
										int pType = dataIn.readInt();
										int iCount = dataIn.readInt();
										int vCount = dataIn.readInt();
										int pMaterialId = dataIn.readInt();
										int dataSize = 0;
										
										if(MODE_VERBOSE) System.out.println("            > type: "+pType);
										if(MODE_VERBOSE) System.out.println("            > input: "+iCount);
										if(MODE_VERBOSE) System.out.println("            > vertex: "+vCount);
										if(MODE_VERBOSE) System.out.println("            > materialId: "+((pMaterialId == 0)? "none":pMaterialId));
										
										//[$SEMANTIC][$SIZE][$OFFSET]...[$SEMANTIC][$SIZE][$OFFSET]
										for(int iIndex=0; iIndex < iCount; iIndex++){
											if(MODE_VERBOSE) System.out.println("            > INPUT");
											int iSemantic = dataIn.readInt();
											int iSet = dataIn.readInt();
											int iSize = dataIn.readInt();
											int iOffset = dataIn.readInt();
											
											if(MODE_VERBOSE) System.out.println("                > semantic: "+iSemantic);
											if(MODE_VERBOSE) System.out.println("                > set: "+iSet);
											if(MODE_VERBOSE) System.out.println("                > size: "+iSize);
											if(MODE_VERBOSE) System.out.println("                > offset: "+iOffset);
											dataSize += vCount * iSize; 
										}
										
										ByteBuffer iBuffer = ByteBuffer.allocateDirect(4*dataSize);
										inChannel.read(iBuffer);
										
										if(MODE_VERBOSE) System.out.println("            > DATA");
										if(MODE_VERBOSE) System.out.println("            > "+iBuffer);
									}
									
									break;
								case API.LIGHT:
									/************************************************************************************************
									 * LIGHT	- Describes the light source in the scene	
									 ************************************************************************************************
									 
									  [LIGHT][$ID][$LIGHT_TYPE]
									  case AMBIENT : [$COLOR_R][$COLOR_G][$COLOR_B]
									  case DIRECTIONAL : [$COLOR_R][$COLOR_G][$COLOR_B]
									  case POINT : [$COLOR_R][$COLOR_G][$COLOR_B][$KC][$KL][$KQ]
									  case SPOT : [$COLOR_R][$COLOR_G][$COLOR_B][$KC][$KL][$KQ][$FALLOFF_ANGLE][$FALLOFF_EXP]
									  ...
									  [LIGHT][$ID]
									  
									 ************************************************************************************************/
									//[LIGHT][$ID][$LIGHT_TYPE]
									if(MODE_VERBOSE) System.out.println("    > LIGHT");
									int lId = dataIn.readInt();
									int lType = dataIn.readInt();
									float lRed = dataIn.readFloat();
									float lGreen = dataIn.readFloat();
									float lblue = dataIn.readFloat();
									
									if(MODE_VERBOSE) System.out.println("        > id: "+lId);
									if(MODE_VERBOSE) System.out.println("        > type: "+lType);
									if(MODE_VERBOSE) System.out.println("        > RGB: "+lRed+" "+lGreen+" "+lblue);
									
									switch(lType){
										case API.POINT:
											float lpKc = dataIn.readFloat();
											float lpKl = dataIn.readFloat();
											float lpKq = dataIn.readFloat();
											if(MODE_VERBOSE) System.out.println("        > KC: "+lpKc);
											if(MODE_VERBOSE) System.out.println("        > KL: "+lpKl);
											if(MODE_VERBOSE) System.out.println("        > KQ: "+lpKq);
											break;
										case API.SPOT:
											float lsKc = dataIn.readFloat();
											float lsKl = dataIn.readFloat();
											float lsKq = dataIn.readFloat();
											float lsCoA = dataIn.readFloat();
											float lsCoE = dataIn.readFloat();
											
											if(MODE_VERBOSE) System.out.println("        > KC: "+lsKc);
											if(MODE_VERBOSE) System.out.println("        > KL: "+lsKl);
											if(MODE_VERBOSE) System.out.println("        > KQ: "+lsKq);
											if(MODE_VERBOSE) System.out.println("        > CutOff Angle: "+lsCoA);
											if(MODE_VERBOSE) System.out.println("        > CutOff Exp: "+lsCoE);
											break;	
									}
										
									break;
								case API.CAMERA:
									/************************************************************************************************
									 * CAMERA	- Describes the optics in the scene	
									 ************************************************************************************************
									 
									  [CAMERA][$ID][$CAMERA_TYPE]
									  case PERSPECTIVE : [$XFOV][$YFOV][$ASPECT_RATIO][$ZNEAR][$ZFAR][$UP_X][$UP_Y][$UP_Z]
									  case ORTHOGRAPHIC : [$XMAG][$YMAG][$ASPECT_RATIO][$ZNEAR][$ZFAR][$UP_X][$UP_Y][$UP_Z]
									  ...
									  [CAMERA][$ID]
									  
									 ************************************************************************************************/
									// [CAMERA][$ID][$CAMERA_TYPE]
									if(MODE_VERBOSE) System.out.println("    > CAMERA");
									int cId = dataIn.readInt();
									int cType = dataIn.readInt();
									
									if(MODE_VERBOSE) System.out.println("        > id: "+cId);
									if(MODE_VERBOSE) System.out.println("        > type: "+cType);
									
									switch(cType){
										case API.PERSPECTIVE:
											final float cpXFov = dataIn.readFloat();
											final float cpYFov = dataIn.readFloat();
											final float cpRatio = dataIn.readFloat();
											final float cpZNear = dataIn.readFloat();
											final float cpZfar = dataIn.readFloat();
											final float[] cpUp = new float[3];
											cpUp[0] = dataIn.readFloat();
											cpUp[1] = dataIn.readFloat();
											cpUp[2] = dataIn.readFloat();
											if(MODE_VERBOSE) System.out.println("        > xFov: "+cpXFov);
											if(MODE_VERBOSE) System.out.println("        > yFov: "+cpYFov);
											if(MODE_VERBOSE) System.out.println("        > ratio: "+cpRatio);
											if(MODE_VERBOSE) System.out.println("        > zNear: "+cpZNear);
											if(MODE_VERBOSE) System.out.println("        > zFar: "+cpZfar);
											if(MODE_VERBOSE) System.out.println("        > up : "+cpUp[0]+" "+cpUp[1]+" "+cpUp[2]);
											break;
										case API.ORTHOGRAPHIC:
											final float coXMag = dataIn.readFloat();
											final float coYMag = dataIn.readFloat();
											final float coRatio = dataIn.readFloat();
											final float coZNear = dataIn.readFloat();
											final float coZfar = dataIn.readFloat();
											final float[] coUp = new float[3];
											coUp[0] = dataIn.readFloat();
											coUp[1] = dataIn.readFloat();
											coUp[2] = dataIn.readFloat();
											if(MODE_VERBOSE) System.out.println("        > xMag: "+coXMag);
											if(MODE_VERBOSE) System.out.println("        > yMag: "+coYMag);
											if(MODE_VERBOSE) System.out.println("        > ratio: "+coRatio);
											if(MODE_VERBOSE) System.out.println("        > zNear: "+coZNear);
											if(MODE_VERBOSE) System.out.println("        > zFar: "+coZfar);
											if(MODE_VERBOSE) System.out.println("        > up : "+coUp[0]+" "+coUp[1]+" "+coUp	[2]);
											break;	
									}
									break;
								case API.IMAGERY:
									/************************************************************************************************
									 * IMAGERY	- Describes the images in the scene	
									 ************************************************************************************************
									 
									  [IMAGE][$ID][$FORMAT][$INTERNAL_FORMAT][$TYPE][$WIDTH][$HEIGHT][$COMPRESS][$PATH]
									  ...
									  [IMAGE][$ID][$FORMAT][$INTERNAL_FORMAT][$TYPE][$WIDTH][$HEIGHT][$COMPRESS][$PATH]
									  
									 ************************************************************************************************/
									if(MODE_VERBOSE) System.out.println("    > IMAGE");
									int iId = dataIn.readInt();
									int iFormat = dataIn.readInt();
									int iiFormat = dataIn.readInt();
									int iType = dataIn.readInt();
									int iWidth = dataIn.readInt();
									int iHeight = dataIn.readInt();
									boolean iCompress = dataIn.readBoolean();
									int iMag = dataIn.readInt();
									int iMin = dataIn.readInt();
									stringBuilder.delete(0, stringBuilder.length());
									char tmpChar;
									while((tmpChar = dataIn.readChar()) != '\0'){
										stringBuilder.append(tmpChar);
									}
									if(MODE_VERBOSE) System.out.println("        > id: "+iId);
									if(MODE_VERBOSE) System.out.println("        > format: "+iFormat);
									if(MODE_VERBOSE) System.out.println("        > internal format: "+iiFormat);
									if(MODE_VERBOSE) System.out.println("        > type: "+iType);
									if(MODE_VERBOSE) System.out.println("        > width: "+iWidth);
									if(MODE_VERBOSE) System.out.println("        > height: "+iHeight);
									if(MODE_VERBOSE) System.out.println("        > compress: "+iCompress);
									if(MODE_VERBOSE) System.out.println("        > mag: "+iMag);
									if(MODE_VERBOSE) System.out.println("        > min: "+iMin);
									if(MODE_VERBOSE) System.out.println("        > path: "+stringBuilder);
									
									break;
								case API.MATERIAL:
									/************************************************************************************************
									 * MATERIAL	- Describes the materials in the scene	
									 ************************************************************************************************
									 
									  [MATERIAL][$ID][$MATERIAL_TYPE]
										  case CONSTANT : [$EMISSION_VAR_TYPE][$EMISSION_VALUE]
										  case DIFFUSE :  [$VAR_TYPE][$EMISSION_VALUE]
										  				  [$AMBIENT_VAR_TYPE][$AMBIENT_VALUE]
										  				  [$DIFFUSE_VAR_TYPE][$DIFFUSE_VALUE]
										  case SPECULAR : [$VAR_TYPE][$EMISSION_VALUE]
										  				  [$AMBIENT_VAR_TYPE][$AMBIENT_VALUE]
										  				  [$DIFFUSE_VAR_TYPE][$DIFFUSE_VALUE]
										  				  [$SPECULAR_VAR_TYPE][$SPECULAR_VALUE]
										  				  [$SHININESS_VAR_TYPE][$SHININESS_VALUE]
									  ...
									  [MATERIAL][$ID][$MATERIAL_TYPE]
									  
									   $*_VAR_TYPE :
									  	case FLOAT : [$FLOAT_VALUE]
									  	case FLOAT3 : [$FLOAT_VALUE_0][$FLOAT_VALUE_1][$FLOAT_VALUE_2]
									  	case FLOAT4 : [$FLOAT_VALUE_0][$FLOAT_VALUE_1][$FLOAT_VALUE_2][$FLOAT_VALUE_3]
									  	case SAMPLER : [$IMAGERY_ID][$SEMANTIC]
									  	case UNSPECIFIED : -
									 ************************************************************************************************/
									if(MODE_VERBOSE) System.out.println("    > MATERIAL");
									final int mId = dataIn.readInt();
									final int mType = dataIn.readInt();
									
									if(MODE_VERBOSE) System.out.println("        > id: "+mId);
									if(MODE_VERBOSE) System.out.println("        > type: "+mType);
									final int mEmissionVarType = dataIn.readInt();
									if(MODE_VERBOSE) System.out.print("        > Emission : "+mEmissionVarType +" -> ");
									final int mEmissionSample;
									final int mEmissionSemantic;
									final float[] mEmissionColor;
									switch(mEmissionVarType){
										case API.FLOAT4 :
											mEmissionColor = new float[4];
											mEmissionColor[0] = dataIn.readFloat();
											mEmissionColor[1] = dataIn.readFloat();
											mEmissionColor[2] = dataIn.readFloat();
											mEmissionColor[3] = dataIn.readFloat();
											mEmissionSample = API.UNSPECIFIED;
											if(MODE_VERBOSE) System.out.println(mEmissionColor[0]+" "+mEmissionColor[1]+" "+mEmissionColor[2]+" "+mEmissionColor[3]);
											break;
										case API.SAMPLER :
											mEmissionColor = null;
											mEmissionSample = dataIn.readInt();
											if(MODE_VERBOSE) System.out.print(mEmissionSample);
											mEmissionSemantic = dataIn.readInt();
											if(MODE_VERBOSE) System.out.println(" - Semantic : " + mEmissionSemantic);
											break;
										default : 
											mEmissionColor = null;
											mEmissionSample = API.UNSPECIFIED;
											if(MODE_VERBOSE) System.out.println("not set");
									}
									final int mReflectiveVarType = dataIn.readInt();
									if(MODE_VERBOSE) System.out.print("        > Reflective : "+mReflectiveVarType +" -> ");
									final int mReflectiveSample;
									final int mReflectiveSemantic;
									final float[] mReflectiveColor;
									switch(mReflectiveVarType){
										case API.FLOAT4 :
											mReflectiveColor = new float[4];
											mReflectiveColor[0] = dataIn.readFloat();
											mReflectiveColor[1] = dataIn.readFloat();
											mReflectiveColor[2] = dataIn.readFloat();
											mReflectiveColor[3] = dataIn.readFloat();
											mReflectiveSample = API.UNSPECIFIED;
											if(MODE_VERBOSE) System.out.println(mReflectiveColor[0]+" "+mReflectiveColor[1]+" "+mReflectiveColor[2]+" "+mReflectiveColor[3]);
											break;
										case API.SAMPLER :
											mReflectiveColor = null;
											mReflectiveSample = dataIn.readInt();
											if(MODE_VERBOSE) System.out.print(mReflectiveSample);
											mReflectiveSemantic = dataIn.readInt();
											if(MODE_VERBOSE) System.out.println(" - Semantic : " + mReflectiveSemantic);
											break;
										default :
											mReflectiveColor = null;
											mReflectiveSample = API.UNSPECIFIED;
											if(MODE_VERBOSE) System.out.println("not set");
									}
									final int mReflectivityVarType = dataIn.readInt();
									if(MODE_VERBOSE) System.out.print("        > Reflectivity : "+mReflectivityVarType +" -> ");
									final float mReflectivity;
									if(mReflectivityVarType == API.FLOAT){
										mReflectivity = dataIn.readFloat();
										if(MODE_VERBOSE) System.out.println(mReflectivity);
									}
									else{
										mReflectivity = API.UNSPECIFIED;
										if(MODE_VERBOSE) System.out.println("not set");
									}
									final int mTransparentVarType = dataIn.readInt();
									if(MODE_VERBOSE) System.out.print("        > Transparent : "+mTransparentVarType +" -> ");
									final int mTransparentSample;
									final int mTransparentSemantic;
									final float[] mTransparentColor;
									switch(mTransparentVarType){
										case API.FLOAT4 :
											mTransparentColor = new float[4];
											mTransparentColor[0] = dataIn.readFloat();
											mTransparentColor[1] = dataIn.readFloat();
											mTransparentColor[2] = dataIn.readFloat();
											mTransparentColor[3] = dataIn.readFloat();
											mTransparentSample = API.UNSPECIFIED;
											if(MODE_VERBOSE) System.out.println(mTransparentColor[0]+" "+mTransparentColor[1]+" "+mTransparentColor[2]+" "+mTransparentColor[3]);
											break;
										case API.SAMPLER :
											mTransparentColor = null;
											mTransparentSample = dataIn.readInt();
											if(MODE_VERBOSE) System.out.print(mTransparentSample);
											mTransparentSemantic = dataIn.readInt();
											if(MODE_VERBOSE) System.out.println(" - Semantic : " + mTransparentSemantic);
											break;
										default :
											mTransparentColor = null;
											mTransparentSample = API.UNSPECIFIED;
											if(MODE_VERBOSE) System.out.println("not set");
									}
									final int mTransparencyVarType = dataIn.readInt();
									if(MODE_VERBOSE) System.out.print("        > Transparency : "+mTransparencyVarType +" -> ");
									final float mTransparency;
									if(mTransparencyVarType == API.FLOAT){
										mTransparency = dataIn.readFloat();
										if(MODE_VERBOSE) System.out.println(mTransparency);
									}
									else{
										mTransparency = API.UNSPECIFIED;
										if(MODE_VERBOSE) System.out.println("not set");
									}
									final int mRefractionVarType = dataIn.readInt();
									if(MODE_VERBOSE) System.out.print("        > Refraction : "+mRefractionVarType +" -> ");
									final float mRefraction;
									if(mRefractionVarType == API.FLOAT){
										mRefraction = dataIn.readFloat();
										if(MODE_VERBOSE) System.out.println(mRefraction);
									}
									else{
										mRefraction = API.UNSPECIFIED;
										if(MODE_VERBOSE) System.out.println("not set");
									}
									final int mAmbientVarType = dataIn.readInt();
									if(MODE_VERBOSE) System.out.print("        > Ambient : "+mAmbientVarType +" -> ");
									final int mAmbientSample;
									final int mAmbientSemantic;
									final float[] mAmbientColor;
									switch(mAmbientVarType){
										case API.FLOAT4 :
											mAmbientColor = new float[4];
											mAmbientColor[0] = dataIn.readFloat();
											mAmbientColor[1] = dataIn.readFloat();
											mAmbientColor[2] = dataIn.readFloat();
											mAmbientColor[3] = dataIn.readFloat();
											mAmbientSample = API.UNSPECIFIED;
											if(MODE_VERBOSE) System.out.println(mAmbientColor[0]+" "+mAmbientColor[1]+" "+mAmbientColor[2]+" "+mAmbientColor[3]);
											break;
										case API.SAMPLER :
											mAmbientColor = null;
											mAmbientSample = dataIn.readInt();
											if(MODE_VERBOSE) System.out.print(mAmbientSample);
											mAmbientSemantic = dataIn.readInt();
											if(MODE_VERBOSE) System.out.println(" - Semantic : " + mAmbientSemantic);
											break;
										default : 
											mAmbientColor = null;
											mAmbientSample = API.UNSPECIFIED;
											if(MODE_VERBOSE) System.out.println("not set");
									}
									final int mDiffuseVarType = dataIn.readInt();
									if(MODE_VERBOSE) System.out.print("        > Diffuse : "+mDiffuseVarType +" -> ");
									final int mDiffuseSample;
									final int mDiffuseSemantic;
									final float[] mDiffuseColor;
									switch(mDiffuseVarType){
										case API.FLOAT4 :
											mDiffuseColor = new float[4];
											mDiffuseColor[0] = dataIn.readFloat();
											mDiffuseColor[1] = dataIn.readFloat();
											mDiffuseColor[2] = dataIn.readFloat();
											mDiffuseColor[3] = dataIn.readFloat();
											mDiffuseSample = API.UNSPECIFIED;
											if(MODE_VERBOSE) System.out.println(mDiffuseColor[0]+" "+mDiffuseColor[1]+" "+mDiffuseColor[2]+" "+mDiffuseColor[3]);
											break;
										case API.SAMPLER :
											mDiffuseColor = null;
											mDiffuseSample = dataIn.readInt();
											if(MODE_VERBOSE) System.out.print(mDiffuseSample);
											mDiffuseSemantic = dataIn.readInt();
											if(MODE_VERBOSE) System.out.println(" - Semantic : " + mDiffuseSemantic);
											break;
										default :
											mDiffuseColor = null;
											mDiffuseSample = API.UNSPECIFIED;
											if(MODE_VERBOSE) System.out.println("not set");
									}
									final int mSpecularVarType = dataIn.readInt();
									if(MODE_VERBOSE) System.out.print("        > Specular : "+mSpecularVarType +" -> ");
									final int mSpecularSample;
									final int mSpecularSemantic;
									final float[] mSpecularColor;
									switch(mSpecularVarType){
										case API.FLOAT4 :
											mSpecularColor = new float[4];
											mSpecularColor[0] = dataIn.readFloat();
											mSpecularColor[1] = dataIn.readFloat();
											mSpecularColor[2] = dataIn.readFloat();
											mSpecularColor[3] = dataIn.readFloat();
											mSpecularSample = API.UNSPECIFIED;
											if(MODE_VERBOSE) System.out.println(mSpecularColor[0]+" "+mSpecularColor[1]+" "+mSpecularColor[2]+" "+mSpecularColor[3]);
											break;
										case API.SAMPLER :
											mSpecularColor = null;
											mSpecularSample = dataIn.readInt();
											if(MODE_VERBOSE) System.out.print(mSpecularSample);
											mSpecularSemantic = dataIn.readInt();
											if(MODE_VERBOSE) System.out.println(" - Semantic : " + mSpecularSemantic);
											break;
										default :
											mSpecularColor = null;
											mSpecularSample = API.UNSPECIFIED;
											if(MODE_VERBOSE) System.out.println("not set");
									}
									final int mShininessVarType = dataIn.readInt();
									if(MODE_VERBOSE) System.out.print("        > Shininess : "+mShininessVarType +" -> ");
									final float mShininess;
									if(mShininessVarType == API.FLOAT){
										mShininess = dataIn.readFloat();
										if(MODE_VERBOSE) System.out.println(mShininess);
									}
									else{
										mShininess = API.UNSPECIFIED;
										if(MODE_VERBOSE) System.out.println("not set");
									}
									
									break;	
								case API.NODE:
									/************************************************************************************************
									 * NODE - Describes the nodes element in the scene	
									 ************************************************************************************************
									  
									  [NODE][$ID][$NODE_TYPE][$ELEMENT_COUNT]
									  		if LOOKAT : [LOOKAT][$EYE_X][$EYE_Y][$EYE_Z][$INTEREST_X][$INTEREST_Y][$INTEREST_Z][$UP_X][$UP_Y][$UP_Z]
									  		if MATRIX : [MATRIX][$0_0]...[$3_3]
									  		if ROTATE : [ROTATE][$ROTATE_X][$ROTATE_Y][$ROTATE_Z][$ROTATE_DEGRE]
									  		if SCALE : [SCALE][$SCALE_X][$SCALE_Y][$SCALE_Z]
									  		if SKEW : [SKEW][$ROTATE_DEGRE][$ROTATE_X][$ROTATE_Y][$ROTATE_Z][$TRANSLATE_X][$TRANSLATE_Y][$TRANSLATE_Z]
									  		if TRANSLATE : [TRANSLATE][$TRANSLATE_X][$TRANSLATE_Y][$TRANSLATE_Z]
									  		if CAMERA : [CAMERA][$CAMERA_ID]
									  		if GEOMETRY : [GEOMETRY][$GEOMETRY_ID][$MATERIAL_COUNT]
									  			[$MATERIAL_ID][$MATERIAL_TARGET_ID][$BIND_COUNT]
									  				[$INPUT_SEMANTIC][$INPUT_SET][$MATERIAL_SEMANTIC]
									  				...
									  				[$INPUT_SEMANTIC][$INPUT_SET][$MATERIAL_SEMANTIC]
									  			...
									  			[$MATERIAL_ID][$MATERIAL_TARGET_ID][$BIND_COUNT]
									  		if LIGHT : [LIGHT][$LIGHT_ID]
									  		if NODE : [NODE][$NODE_ID]
									  ...
									  [NODE][$ID][$NODE_TYPE][$ELEMENT_COUNT]
									 ************************************************************************************************/
									if(MODE_VERBOSE) System.out.println("    > NODE");
									final int nId = dataIn.readInt();
									final int nType = dataIn.readInt();
									final int nElements = dataIn.readInt();
									
									if(MODE_VERBOSE) System.out.println("        > id: "+nId);
									if(MODE_VERBOSE) System.out.println("        > type: "+nType);
									if(MODE_VERBOSE) System.out.println("        > elements: "+nElements);
									
									for(int eIndex=0; eIndex < nElements; eIndex++){
										int nodeElement = dataIn.readInt();
										switch(nodeElement){
											case API.LOOKAT:
												float[] lookat = new float[]{dataIn.readFloat(),dataIn.readFloat(),dataIn.readFloat(),dataIn.readFloat(),dataIn.readFloat(),dataIn.readFloat(),dataIn.readFloat(),dataIn.readFloat(),dataIn.readFloat()};
												if(MODE_VERBOSE) System.out.println("            > LOOKAT: "+lookat[0]+" "+lookat[1]+" "+lookat[2]+" "+lookat[3]+" "+lookat[4]+" "+lookat[5]+" "+lookat[6]+" "+lookat[7]+" "+lookat[8]);
												break;
											case API.MATRIX:
												float[] matrix = new float[]{dataIn.readFloat(),dataIn.readFloat(),dataIn.readFloat(),dataIn.readFloat(),dataIn.readFloat(),dataIn.readFloat(),dataIn.readFloat(),dataIn.readFloat(),dataIn.readFloat(),dataIn.readFloat(),dataIn.readFloat(),dataIn.readFloat(),dataIn.readFloat(),dataIn.readFloat(),dataIn.readFloat(),dataIn.readFloat()};
												if(MODE_VERBOSE) System.out.println("            > MATRIX: "+matrix[0]+" "+matrix[1]+" "+matrix[2]+" "+matrix[3]+" "+matrix[4]+" "+matrix[5]+" "+matrix[6]+" "+matrix[7]+" "+matrix[8]+" "+matrix[9]+" "+matrix[10]+" "+matrix[11]+" "+matrix[12]+" "+matrix[13]+" "+matrix[14]+" "+matrix[15]);
												break;
											case API.ROTATE:
												float[] rotate = new float[]{dataIn.readFloat(),dataIn.readFloat(),dataIn.readFloat(),dataIn.readFloat()};
												if(MODE_VERBOSE) System.out.println("            > ROTATE: "+rotate[0]+" "+rotate[1]+" "+rotate[2]+" "+rotate[3]);												
												break;
											case API.SCALE:
												float[] scale = new float[]{dataIn.readFloat(),dataIn.readFloat(),dataIn.readFloat()};
												if(MODE_VERBOSE) System.out.println("            > SCALE: "+scale[0]+" "+scale[1]+" "+scale[2]);
												break;
											case API.SKEW:
												float[] skew = new float[]{dataIn.readFloat(),dataIn.readFloat(),dataIn.readFloat(),dataIn.readFloat(),dataIn.readFloat(),dataIn.readFloat()};
												if(MODE_VERBOSE) System.out.println("            > SKEW: "+skew[0]+" "+skew[1]+" "+skew[2]+" "+skew[3]+" "+skew[4]+" "+skew[5]);
												break;
											case API.TRANSLATE:
												float[] translate = new float[]{dataIn.readFloat(),dataIn.readFloat(),dataIn.readFloat()};
												if(MODE_VERBOSE) System.out.println("            > TRANSLATE: "+translate[0]+" "+translate[1]+" "+translate[2]);
												break;
											case API.CAMERA:
												int cameraId = dataIn.readInt();
												if(MODE_VERBOSE) System.out.println("            > CAMERA: "+cameraId);
												break;
											case API.GEOMETRY:
												int geometryId = dataIn.readInt();
												int materialCount = dataIn.readInt();
												if(MODE_VERBOSE) System.out.println("            > GEOMETRY: "+geometryId+" ["+materialCount+" materials]");
												for(int mIndex=0; mIndex < materialCount; mIndex++){
													int materialId = dataIn.readInt();
													int materialTargetId = dataIn.readInt();
													int bindCount = dataIn.readInt();
													if(MODE_VERBOSE) System.out.println("                > MATERIAL: "+materialId+" on "+materialTargetId+" ["+bindCount+" binds]");
													for(int bIndex=0; bIndex < bindCount; bIndex++){
														int biSemantic = dataIn.readInt();
														int biSet = dataIn.readInt();
														int bmSemantic = dataIn.readInt();
														if(MODE_VERBOSE) System.out.println("                    > BIND : "+bmSemantic+" on "+biSemantic + " - set : "+biSet);
													}
												}
												break;
											case API.LIGHT:
												int lightId = dataIn.readInt();
												if(MODE_VERBOSE) System.out.println("            > LIGHT: "+lightId);
												break;
											case API.NODE:
												int nodeId = dataIn.readInt();
												if(MODE_VERBOSE) System.out.println("            > NODE: "+nodeId);
												break;
											default:
												throw new IOException("Unsupported node element "+nodeElement);
										}
									}
									break;
								case API.SCENE:
									/************************************************************************************************
									 * SCENE - Describes the scene elements	
									 ************************************************************************************************
									  
									  [SCENE][$ID][$NODE_COUNT]
									  [NODE_ID]
									  ...		
									  [SCENE][$ID][$NODE_COUNT]
									 ************************************************************************************************/
									if(MODE_VERBOSE) System.out.println("    > SCENE");
									int sceneId = dataIn.readInt();
									int nodeCount = dataIn.readInt();
									
									if(MODE_VERBOSE) System.out.println("        > id: "+sceneId);
									if(MODE_VERBOSE) System.out.println("        > nodes: "+nodeCount);
									
									for(int nIndex=0; nIndex < nodeCount; nIndex++){
										int nodeId = dataIn.readInt();
										if(MODE_VERBOSE) System.out.println("            > "+nodeId);
									}
									break;
								default:
									throw new IOException("Unsupported entry type "+entryType);
							}
						}
						
						System.out.println("Parsing time : " + (System.currentTimeMillis() - startTime)+"ms");
						
					}catch(Exception e){
						e.printStackTrace();
					}
				}
				else{
					System.err.println("    [ERROR] File not found : "+inFile.getPath());
				}
			}
		}

	}

}
