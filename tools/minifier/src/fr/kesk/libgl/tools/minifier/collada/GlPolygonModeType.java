//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.01.22 at 10:23:29 PM CET 
//


package fr.kesk.libgl.tools.minifier.collada;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for gl_polygon_mode_type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="gl_polygon_mode_type">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="POINT"/>
 *     &lt;enumeration value="LINE"/>
 *     &lt;enumeration value="FILL"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "gl_polygon_mode_type")
@XmlEnum
public enum GlPolygonModeType {

    POINT,
    LINE,
    FILL;

    public String value() {
        return name();
    }

    public static GlPolygonModeType fromValue(String v) {
        return valueOf(v);
    }

}
