#!/bin/sh

#To add dependencies :
#	- add native keyword to method of class
#	- create header using javah (see GLES20Utils)
#	- implement method in .c
#	- add .c to Android.mk
#	- native.sh
#	- in JAVA -> add static call : System.loadLibrary("LGL");
 

cd bin/classes
if [ ! -f ../../jni/GLES20Utils.h ]; then
	javah -jni -o ../../jni/GLES20Utils.h fr.kesk.libgl.tools.GLES20Utils
fi

if [ ! -f ../../jni/MatrixUtils.h ]; then
	javah -jni -o ../../jni/MatrixUtils.h fr.kesk.libgl.tools.MatrixUtils
fi

if [ ! -f ../../jni/NodeUtils.h ]; then
	javah -jni -o ../../jni/NodeUtils.h fr.kesk.libgl.tools.NodeUtils
fi

cd ../..
../../../Tools/android-ndk-r8b/ndk-build 